<?php

namespace App\Http\Controllers;

use App\Models\Localidades;
use App\Models\Alumnos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlumnosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $localidades = Localidades::all();

        return view('Alumnos.index', ['alumnos' => DB::table('alumnos')->paginate(10)], ['localidades' => $localidades]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $localidades = Localidades::all();

        return view('Alumnos.create', ['localidades' => $localidades]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([

            'nombre'  => 'required',

            'apellido_1' => 'required',

            'apellido_2' => 'required',

            'DNI' => 'required',

            'telefono'  => 'required',

            'localidades_id'  => 'required',

        ]);

        Alumnos::create($request->all());

        return redirect()->route('Alumnos.index')->with('success', 'Nuevo alumno dado de alta.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     */
    public function show(Alumnos $alumnos)
    {
        return redirect('/inicio');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $alumno = Alumnos::findOrFail($id);

        $localidades = Localidades::all();

        return view('Alumnos.edit', ['alumno' => $alumno, 'localidades' => $localidades]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array_slice($request->all(), 2);

        Alumnos::whereId($id)->update($data);

        $request->session()->flash('correcto', 'Se ha editado el registro.');

        return redirect('Alumnos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     * @param  \Illuminate\Http\Request  $request
     */
    public function destroy($id, Request $request)
    {
        
        Alumnos::where('id', $id)->delete();

        $request->session()->flash('correcto', 'Se ha borrado el registro.');

        return redirect('Alumnos');

    }
}
