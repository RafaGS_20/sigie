<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alumnos;
use App\Models\AlumnosEmpresasJornadas;
use App\Models\Empresas;
use App\Models\Cursos;
use App\Models\Jornadas;
use Illuminate\Support\Facades\DB;

class Alumnos_Empresas_JornadasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/inicio');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $alumnos = Alumnos::all();

        $empresas = Empresas::all();

        $id_jornada = DB::table('jornadas')->max('id');

        $jornadas = Jornadas::whereId($id_jornada)->get();

        $cantidades = Cursos::whereId($jornadas[0]['cursos_id'])->get();

        $cantidad = intval($cantidades[0]['num_alum_max']);

        return view('Vinculo.create', ['alumnos' => $alumnos, 'empresas' => $empresas, 'jornada' => $jornadas, 'cantidad' => $cantidad]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();

        $datos = ( (count($data)/2) - 1);

        $jornadas_id = DB::table('jornadas')->max('id');

        $datos_vacios = 0;

        for ($i = 1; $i < $datos; $i++) {

            if( ($data['alumnos_id_'.$i]) != null && ($data['empresas_id_'.$i] != null)){

                
                AlumnosEmpresasJornadas::create([

                    'alumnos_id' => $data['alumnos_id_' . $i],
    
                    'empresas_id' => $data['empresas_id_' . $i],
    
                    'jornadas_id' => $jornadas_id
    
                ]);

            } else {

               
                $datos_vacios++;

            }

        }

        return redirect('/calendario');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('/inicio');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect('/inicio');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect('/inicio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('/inicio');
    }
}
