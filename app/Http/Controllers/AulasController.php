<?php

namespace App\Http\Controllers;

use App\Models\Aulas;
use App\Models\Centros;
use Illuminate\Http\Request;

class AulasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/inicio');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $centros = Centros::all();

        return view('Aulas.create',  ['centros' => $centros]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Aulas::create($request->all());

        return redirect()->route('Centros.index')->with('success', 'Nuevo centro dado de alta.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Aulas  $aulas
     * @return \Illuminate\Http\Response
     */
    public function show(Aulas $aulas)
    {
        return redirect('/inicio');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $aula = Aulas::findOrFail($id);

        $centros = Centros::all();

        return view('Aulas.edit', ['aula' => $aula], ['centros' => $centros]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = array_slice($request->all(), 2);

        Aulas::whereId($id)->update($data);

        $request->session()->flash('correcto', 'Se ha editado el registro.');

        return redirect('Centros');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Aulas  $aulas
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @param  \Illuminate\Http\Request  $request
     */
    public function destroy($id, Request $request)
    {
        Aulas::where('id', $id)->delete();

        $request->session()->flash('correcto', 'Se ha borrado el registro.');

        return redirect('Centros');
        
    }
}
