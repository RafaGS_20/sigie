<?php

namespace App\Http\Controllers;

use App\Models\Centros;
use App\Models\Jornadas;
use App\Models\Localidades;
use App\Models\Aulas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CentrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $localidades = Localidades::all();

        return view('Centros.index',  ['centros' => DB::table('centros')->paginate(10)], ['localidades' => $localidades]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $localidades = Localidades::all();

        return view('Centros.create', ['localidades' => $localidades]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Centros::create($request->all());

        return redirect()->route('Aulas.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $centro = Centros::findOrFail($id);

        $aulas = Aulas::where('centros_id', '=', $id)->get();

        $localidades = Localidades::all();

        return view(

            'Centros.show',

            [

                'centro' => $centro,

                'aulas' => $aulas,

                'localidades' => $localidades,

            ]

        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $centro = Centros::findOrFail($id);

        $localidades = Localidades::all();

        return view('Centros.edit', ['centro' => $centro], ['localidades' => $localidades]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array_slice($request->all(), 2);

        Centros::whereId($id)->update($data);

        $request->session()->flash('correcto', 'Se ha editado el registro.');

        return redirect('Centros');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Centros  $centros
     * @return \Illuminate\Http\Response
     * @param  \Illuminate\Http\Request  $request
     */
    public function destroy($id, Request $request)
    {
        Centros::where('id', $id)->delete();

        $request->session()->flash('correcto', 'Se ha borrado el registro.');

        return redirect('Centros');
    }
}
