<?php

namespace App\Http\Controllers;

use App\Models\Cursos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CursosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('Cursos.index',  ['cursos' => DB::table('cursos')->paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cursos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([

            'nombre'  => 'required',

            'horas' => 'required',

            'poliza' => 'required',

            'objetivos' => 'required',

            'contenidos'  => 'required'

        ]);

        Cursos::create($request->all());

        return redirect()->route('Cursos.index')->with('success', 'Nuevo curso dado de alta.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function show(Cursos $cursos)
    {
        return redirect('/inicio');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curso = Cursos::findOrFail($id);

        return view('Cursos.edit', ['curso' => $curso]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $data = array_slice($request->all(), 2);

        Cursos::whereId($id)->update($data);

        $request->session()->flash('correcto', 'Se ha editado el registro.');

        return redirect('Cursos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cursos  $cursos
     * @return \Illuminate\Http\Response
     * @param  \Illuminate\Http\Request  $request
     */
    public function destroy($id, Request $request)
    {
        Cursos::where('id', $id)->delete();

        $request->session()->flash('correcto', 'Se ha borrado el registro.');

        return redirect('Cursos');
    }
}
