<?php

namespace App\Http\Controllers;

use App\Models\Docentes;
use App\Models\Jornadas;
use App\Models\Localidades;
use App\Models\Titulaciones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DocentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $localidades = Localidades::all();

        $titulaciones = Titulaciones::all();

        return view('Docentes.index',  ['docentes' => DB::table('docentes')->paginate(10)], [ 'localidades' => $localidades, 'titulaciones' => $titulaciones]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $localidades = Localidades::all();

        $titulaciones = Titulaciones::all();

        return view('Docentes.create', ['localidades' => $localidades, 'titulaciones' => $titulaciones]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([

            'nombre'  => 'required',

            'apellido_1' => 'required',

            'apellido_2' => 'required',

            'DNI' => 'required',

            'telefono'  => 'required',

            'localidades_id'  => 'required',

        ]);

        Docentes::create($request->all());

        return redirect()->route('Docentes.index')->with('success', 'Nuevo docente dado de alta.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Docentes  $docentes
     * @return \Illuminate\Http\Response
     */
    public function show(Docentes $docentes)
    {
        return redirect('/inicio');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $docente = Docentes::findOrFail($id);

        $localidades = Localidades::all();

        $titulaciones = Titulaciones::all();

        return view('Docentes.edit', ['docente' => $docente, 'localidades' => $localidades, 'titulaciones' => $titulaciones]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array_slice($request->all(), 2);

        Docentes::whereId($id)->update($data);

        $request->session()->flash('correcto', 'Se ha editado el registro.');

        return redirect('Docentes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Docentes  $docentes
     * @return \Illuminate\Http\Response
     * @param  \Illuminate\Http\Request  $request
     */
    public function destroy($id, Request $request)
    {
        Docentes::where('id', $id)->delete();

        $request->session()->flash('correcto', 'Se ha borrado el registro.');

        return redirect('Docentes');
    }
}
