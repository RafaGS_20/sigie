<?php

namespace App\Http\Controllers;

use App\Models\Jornadas;
use App\Models\Cursos;
use App\Models\Aulas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FullCalendarController extends Controller
{
    public function index()
    {

        $data = DB::table('jornadas')->select('cursos_id', 'fecha_ini as start', 'fecha_fin as end', 'id')->get();

        foreach ($data as $event) {

            $curso = Cursos::find($event->cursos_id);

            $event->title = ($curso->nombre);
        }

        return Response()->json($data);
    }

    public function sacaAulas($centro)
    {
        $aulas = Aulas::where('centros_id', '=', $centro)->get();
        return with(["aulas" => $aulas]);
    }
}
