<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\AlumnosExport;
use App\Imports\AlumnosImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportExportController extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function importExportView()
    {
       return view('import');
    }
     
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        return Excel::download(new AlumnosExport, 'Alumnos.xlsx');
    }
     
    /**
    * @return \Illuminate\Support\Collection
    */
    public function import() 
    {
        Excel::import(new AlumnosImport,request()->file('file'));
             
        return back();
        
    }
}
