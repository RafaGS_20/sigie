<?php

namespace App\Http\Controllers;

use App\Models\Jornadas;
use App\Models\Docentes;
use App\Models\Cursos;
use App\Models\Centros;
use App\Models\Sesiones;
use App\Models\Localidades;
use App\Models\Titulaciones;
use App\Models\Aulas;
use App\Models\Alumnos;
use App\Models\Empresas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JornadasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $docentes = Docentes::all();

        $cursos = Cursos::all();

        $centros = Centros::all();

        $sesiones = Sesiones::all();

        $aulas = Aulas::all();

        $jornadas = Jornadas::all();

        return view(
            'Jornadas.index',

            ['jornadas' => DB::table('jornadas')->paginate(15)],

            [

                'docentes' => $docentes,

                'cursos' => $cursos,

                'centros' => $centros,

                'sesiones' => $sesiones,

                'aulas' => $aulas,

                'jornadas' => $jornadas

            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $centros = Centros::all();

        $docentes = Docentes::all();

        $cursos = Cursos::all();

        $aulas = Aulas::all();

        return view('Jornadas.create', [

            'docentes' => $docentes,

            'cursos' => $cursos,

            'centros' => $centros,

            'aulas' => $aulas
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([

            'fecha_ini'  => 'required',

            'fecha_fin' => 'required',

            'num_sesiones' => 'required',

            'num_expediente' => 'required',

            'entidad_organizadora'  => 'required',

            'nombre_comercial'  => 'required',

            'horario' => 'required'

        ]);

        Jornadas::create($request->all());

        return redirect()->route('Sesiones.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Datos generales de la jornada. 

        $jornada = Jornadas::findOrFail($id);

        $curso = Cursos::findOrFail($jornada->cursos_id);

        $sesiones = Sesiones::where('jornadas_id', '=', $jornada->id)->get();

        // Datos del centro. 

        $centro = Centros::findOrFail($jornada->centros_id);

        $aulas = Aulas::where('id', '=', $jornada->aulas_id)->get();

        $localidad_centro = Localidades::findOrFail($centro->localidades_id);

        //Datos del docente. 

        $docente = Docentes::findOrFail($jornada->docentes_id);

        $localidad_docente = Localidades::findOrFail($docente->localidades_id);

        $titulacion_docente = Titulaciones::findOrFail($docente->titulaciones_id);

        // Datos de todos los alumnos y su empresa. 

        $alumnos_empresas = DB::table('alumnos_empresas_jornadas')->where('jornadas_id', '=', $id)->get();

        $empresas = Empresas::all();

        $alumnos = Alumnos::all();

        return view(

            'Jornadas.show',

            [

                'jornada' => $jornada,

                'curso' => $curso,

                'sesiones' => $sesiones,

                'centro' => $centro,

                'aulas' => $aulas,

                'localidad_centro' => $localidad_centro,

                'docente' => $docente,

                'localidad_docente' => $localidad_docente,

                'titulacion_docente' => $titulacion_docente,

                'alumnos_empresas' => $alumnos_empresas,

                'empresas' => $empresas,

                'alumnos' => $alumnos

            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $docentes = Docentes::all();

        $cursos = Cursos::all();

        $centros = Centros::all();

        $sesiones = Sesiones::all();

        $jornada = Jornadas::findOrFail($id);

        return view(
            'Jornadas.edit',

            [
                'jornada' => $jornada,

                'docentes' => $docentes,

                'cursos' => $cursos,

                'centros' => $centros,

                'sesiones' => $sesiones
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array_slice($request->all(), 2);

        Jornadas::whereId($id)->update($data);

        $request->session()->flash('correcto', 'Se ha editado el registro.');

        return redirect('Jornadas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Jornadas  $jornadas
     * @return \Illuminate\Http\Response
     * @param  \Illuminate\Http\Request  $request
     */
    public function destroy($id, Request $request)
    {
        Jornadas::where('id', $id)->delete();

        $request->session()->flash('correcto', 'Se ha borrado el registro.');

        return redirect('Jornadas');
    }

}
