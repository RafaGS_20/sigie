<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Models\Jornadas;
use App\Models\Docentes;
use App\Models\Cursos;
use App\Models\Centros;
use App\Models\Sesiones;
use App\Models\Localidades;
use App\Models\Titulaciones;
use App\Models\Aulas;
use App\Models\Alumnos;
use App\Models\Empresas;
use Illuminate\Support\Facades\DB;

class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cartel($id)
    {

        // Datos generales de la jornada. 

        $jornada = Jornadas::findOrFail($id);

        $curso = Cursos::findOrFail($jornada->cursos_id);

        // Datos del centro. 

        $centro = Centros::findOrFail($jornada->centros_id);

        $aula = Aulas::findOrFail($jornada->aulas_id);

        //Datos del docente. 

        $docente = Docentes::findOrFail($jornada->docentes_id);

        // Mecanismo de importación. 

        $data = [

            'curso' => $curso->nombre,

            'num_expediente' => $jornada->num_expediente,

            'entidad_organizadora' => $jornada->entidad_organizadora,

            'nombre_comercial' => $jornada->nombre_comercial,

            'fecha_ini' => $jornada->fecha_ini,

            'fecha_fin' => $jornada->fecha_fin,

            'horas' => $curso->horas,

            'modalidad' => $curso->modalidad,

            'docente' => ($docente->nombre . ' ' . $docente->apellido_1 . ' ' . $docente->apellido_2),

            'horario' => $jornada->horario,

            'lugar' => ($centro->nombre . ', ' . $centro->calle_centro . ',número ' . $centro->num . ',piso ' . $centro->piso . ', aula número ' . $aula->num),

            'poliza' => $curso->poliza,

            'objetivos' => $curso->objetivos,

            'contenidos' => $curso->contenidos

        ];

        $pdf = PDF::loadView('PDF.cartel', $data);

        return $pdf->download('cartel.pdf');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function material($id)
    {

        // Datos generales de la jornada. 

        $jornada = Jornadas::findOrFail($id);

        $curso = Cursos::findOrFail($jornada->cursos_id);

        // Datos de todos los alumnos y su empresa. 

        $alumnos_empresas = DB::table('alumnos_empresas_jornadas')->where('jornadas_id', '=', $id)->get();

        $alumnos = Alumnos::all();

        // Mecanismo de importación. 

        $data = [

            'curso' => $curso->nombre,

            'fecha_ini' => $jornada->fecha_ini,

            'fecha_fin' => $jornada->fecha_fin,

            'alumnos' => $alumnos,

            'alumnos_empresas' => $alumnos_empresas

        ];


        $pdf = PDF::loadView('PDF.material', $data);

        return $pdf->download('material.pdf');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function diplomas($id)
    {

        // Datos generales de la jornada. 

        $jornada = Jornadas::findOrFail($id);

        $curso = Cursos::findOrFail($jornada->cursos_id);

        // Datos de todos los alumnos y su empresa. 

        $alumnos_empresas = DB::table('alumnos_empresas_jornadas')->where('jornadas_id', '=', $id)->get();

        $alumnos = Alumnos::all();

        // Mecanismo de importación. 

        $data = [

            'curso' => $curso->nombre,

            'fecha_ini' => $jornada->fecha_ini,

            'fecha_fin' => $jornada->fecha_fin,

            'alumnos' => $alumnos,

            'alumnos_empresas' => $alumnos_empresas

        ];


        $pdf = PDF::loadView('PDF.diplomas', $data);

        return $pdf->download('diplomas.pdf');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function evaluacion($id)
    {

        // Datos generales de la jornada. 

        $jornada = Jornadas::findOrFail($id);

        $curso = Cursos::findOrFail($jornada->cursos_id);

        //Datos del docente. 

        $docente = Docentes::findOrFail($jornada->docentes_id);

        // Datos de todos los alumnos y su empresa. 

        $alumnos_empresas = DB::table('alumnos_empresas_jornadas')->where('jornadas_id', '=', $id)->get();

        $alumnos = Alumnos::all();

        // Mecanismo de importación. 

        $data = [

            'curso' => $curso->nombre,

            'fecha_ini' => $jornada->fecha_ini,

            'fecha_fin' => $jornada->fecha_fin,

            'docente' => ($docente->nombre . ' ' . $docente->apellido_1 . ' ' . $docente->apellido_2),

            'alumnos' => $alumnos,

            'alumnos_empresas' => $alumnos_empresas

        ];

        $pdf = PDF::loadView('PDF.evaluacion', $data);

        return $pdf->download('evaluacion.pdf');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function asistencia($id)
    {

        // Datos generales de la jornada. 

        $jornada = Jornadas::findOrFail($id);

        $curso = Cursos::findOrFail($jornada->cursos_id);

        $sesiones_jornadas = Sesiones::where('jornadas_id', '=', $jornada->id)->get();

        $sesiones = Sesiones::all();

        //Datos del docente. 

        $docente = Docentes::findOrFail($jornada->docentes_id);

        // Datos de todos los alumnos y su empresa. 

        $alumnos_empresas = DB::table('alumnos_empresas_jornadas')->where('jornadas_id', '=', $id)->get();

        $alumnos = Alumnos::all();

        // Mecanismo de importación. 

        $data = [

            'curso' => $curso->nombre,

            'num_expediente' => $jornada->num_expediente,

            'entidad_organizadora' => $jornada->entidad_organizadora,

            'nombre_comercial' => $jornada->nombre_comercial,

            'fecha_ini' => $jornada->fecha_ini,

            'fecha_fin' => $jornada->fecha_fin,

            'modalidad' => $curso->modalidad,

            'docente' => ($docente->nombre . ' ' . $docente->apellido_1 . ' ' . $docente->apellido_2),

            'horario' => $jornada->horario,

            'alumnos' => $alumnos,

            'alumnos_empresas' => $alumnos_empresas,

            'sesiones_jornadas' => $sesiones_jornadas,

            'sesiones' => $sesiones,

        ];

        $pdf = PDF::loadView('PDF.asistencia', $data);

        return $pdf->download('asistencia.pdf');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fotosRGD($id)
    {

        $alumnos_empresas = DB::table('alumnos_empresas_jornadas')->where('jornadas_id', '=', $id)->get();

        $alumnos = Alumnos::all();

        // Mecanismo de importación. 

        $data = [

            'alumnos' => $alumnos,

            'alumnos_empresas' => $alumnos_empresas,

        ];

        $pdf = PDF::loadView('PDF.fotosRGD', $data);

        return $pdf->download('fotosRGD.pdf');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function registroalumno($id)
    {
        // Datos generales de la jornada. 

        $jornada = Jornadas::findOrFail($id);

        $curso = Cursos::findOrFail($jornada->cursos_id);

        $sesiones_jornadas = Sesiones::where('jornadas_id', '=', $jornada->id)->get();

        $sesiones = Sesiones::all();

        //Datos del docente. 

        $docente = Docentes::findOrFail($jornada->docentes_id);

        // Datos de todos los alumnos y su empresa. 

        $alumnos_empresas = DB::table('alumnos_empresas_jornadas')->where('jornadas_id', '=', $id)->get();

        $alumnos = Alumnos::all();

        // Mecanismo de importación. 

        $data = [

            'curso' => $curso->nombre,

            'num_expediente' => $jornada->num_expediente,

            'entidad_organizadora' => $jornada->entidad_organizadora,

            'nombre_comercial' => $jornada->nombre_comercial,

            'fecha_ini' => $jornada->fecha_ini,

            'fecha_fin' => $jornada->fecha_fin,

            'modalidad' => $curso->modalidad,

            'docente' => ($docente->nombre . ' ' . $docente->apellido_1 . ' ' . $docente->apellido_2),

            'horario' => $jornada->horario,

            'alumnos' => $alumnos,

            'alumnos_empresas' => $alumnos_empresas,

            'sesiones_jornadas' => $sesiones_jornadas,

            'sesiones' => $sesiones,

        ];

        $pdf = PDF::loadView('PDF.registroalumno', $data);

        return $pdf->download('registroalumno.pdf');
    }
}
