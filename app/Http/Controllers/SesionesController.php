<?php

namespace App\Http\Controllers;

use App\Models\Sesiones;
use App\Models\Jornadas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SesionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/inicio');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $jornada = DB::table('jornadas')->max('id');

        $num_sesion = DB::table('jornadas')->whereId($jornada)->max('num_sesiones');

        $num_sesiones = intval($num_sesion);

        return view('Sesiones.create', ['jornada' => $jornada, 'num_sesiones' => $num_sesiones]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $datos = (count($data) - 2);

        $jornadas_id = ($data['jornadas_id']);

        for ( $i = 1; $i <= $datos; $i++){

            Sesiones::create([

                'fecha' => $data['fecha_'.$i], 

                'jornadas_id' => $jornadas_id

            ]);

        }

        return redirect()->route('Alumnos_Empresas_Jornadas.create');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sesiones  $sesiones
     * @return \Illuminate\Http\Response
     */
    public function show(Sesiones $sesiones)
    {
        return redirect('/inicio');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sesiones  $sesiones
     * @return \Illuminate\Http\Response
     */
    public function edit(Sesiones $sesiones)
    {
        return redirect('/inicio');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sesiones  $sesiones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sesiones $sesiones)
    {
        return redirect('/inicio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sesiones  $sesiones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sesiones $sesiones)
    {
        return redirect('/inicio');
    }
}
