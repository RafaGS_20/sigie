<?php

namespace App\Http\Controllers;

use App\Models\Titulaciones;
use Illuminate\Http\Request;

class TitulacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/inicio');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Titulaciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Titulaciones::create($request->all());

        return redirect()->route('Docentes.index')->with('success', 'Nueva titulación registrada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Titulaciones  $titulaciones
     * @return \Illuminate\Http\Response
     */
    public function show(Titulaciones $titulaciones)
    {
        return redirect('/inicio');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Titulaciones  $titulaciones
     * @return \Illuminate\Http\Response
     */
    public function edit(Titulaciones $titulaciones)
    {
        return redirect('/inicio');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Titulaciones  $titulaciones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Titulaciones $titulaciones)
    {
        return redirect('/inicio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Titulaciones  $titulaciones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Titulaciones $titulaciones)
    {
        return redirect('/inicio');
    }
}
