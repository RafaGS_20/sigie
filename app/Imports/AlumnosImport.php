<?php

namespace App\Imports;

use App\Models\Alumnos;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;

/* Intento de generar un sistema de importación de datos con comprobación de datos: si existe se actualiza, sino existe se crea. 
Funcional al 100% */

class AlumnosImport implements ToCollection, WithHeadingRow
{

    use Importable;

    /**
     * @param array $row
     * 
     * @return \Illuminate\Database\Eloquent\Model|Null
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            if (!isset($row['dni'])) {
                return null;
            }

            Alumnos::updateOrCreate([
                'DNI' => $row['dni'],
            ], [
                'nombre' => $row['nombre'],
                'apellido_1' => $row['apellido_1'],
                'apellido_2' => $row['apellido_2'],
                'DNI' => $row['dni'],
                'sexo' => $row['sexo'],
                'discapacidad' => $row['discapacidad'],
                'fecha_nacimiento' => $row['fecha_nacimiento'],
                'nivel_estudios' => $row['nivel_estudios'],
                'direccion' => $row['direccion'],
                'codigo_postal' => $row['codigo_postal'], 
                'telefono' => $row['telefono'],
                'email' => $row['email'],
            ]);
        }
    }
}
