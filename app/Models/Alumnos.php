<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alumnos extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [

        'id',

        'nombre',

        'apellido_1',

        'apellido_2',

        'DNI',

        'sexo',

        'discapacidad',

        'fecha_nacimiento',

        'nivel_estudios',

        'direccion',

        'codigo_postal',

        'telefono',

        'email',

        'localidades_id'

    ];

    /**
     * 
     * Se establece una relación 1:N con la localidad. 
     * 
     */
    public function Localidades()
    {
        return $this->hasOne(Localidades::class, 'localidades_id');
    }

    /**
     * 
     * Se establece una relación con la ternaria. 
     * 
     */
    public function Alumnos_Empresas_Jornadas()
    {
        return $this->hasMany(Alumnos_Empresas_Jornadas::class);
    }
}
