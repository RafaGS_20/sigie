<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlumnosEmpresasJornadas extends Model
{
  use HasFactory;

  public $timestamps = false;

  protected $fillable = [

    'alumnos_id',

    'empresas_id',

    'jornadas_id'

  ];

  /**
   * 
   * Ses establece una relación ternaria con los otros modelos.  
   * 
   */
  public function Alumnos()
  {
    return $this->belongsTo(Alumnos::class);
  }

  /**
   * 
   * Ses establece una relación ternaria con los otros modelos.  
   * 
   */
  public function Empresas()
  {
    return $this->belongsTo(Empresas::class);
  }

  /**
   * 
   * Ses establece una relación ternaria con los otros modelos.  
   * 
   */
  public function Jornadas()
  {
    return $this->belongsTo(Jornadas::class);
  }
}
