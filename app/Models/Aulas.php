<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aulas extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [

        'id',

        'num',

        'aforo',

        'equipo',

        'descripcion',

        'centros_id'

    ];

    /**
     * 
     * Se establece una relación 1:N con los centros. 
     * 
     */
    public function Centros()
    {
        return $this->belongsTo(Centros::class);
    }

    /**
     * 
     * Se establece una relación 1:N con las jornadas. 
     * 
     */
    public function Jornadas()
    {
        return $this->belongsTo(Jornadas::class);
    }
}
