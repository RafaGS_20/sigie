<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Centros extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [

        'id',

        'nombre',

        'calle_centro',

        'num',

        'codigo_postal',

        'piso',

        'email',

        'telefono',

        'localidades_id'

    ];

    /**
     * 
     * Se establece una relación 1:N con la localidad. 
     * 
     */
    public function Localidades()
    {
        return $this->hasOne(Localidades::class, 'localidades_id');
    }

    /**
     * 
     * Se establece una relación 1:N con las aulas. 
     * 
     */
    public function Aulas()
    {
        return $this->hasMany(Aulas::class);
    }

    /**
     * 
     * Se establece una relación 1:N con las jornadas. 
     * 
     */
    public function Jornadas()
    {
        return $this->belongsTo(Jornadas::class);
    }
}
