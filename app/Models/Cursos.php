<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cursos extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [

        'id',

        'nombre',

        'horas',

        'titulacion_requerida',

        'modalidad',

        'num_alum_max',

        'num_alum_min',

        'poliza',

        'objetivos',

        'contenidos'

    ];

    /**
     * 
     * Se establece una relación 1:N con las jornadas. 
     * 
     */
    public function Jornadas()
    {
        return $this->hasMany(Jornadas::class);
    }

}
