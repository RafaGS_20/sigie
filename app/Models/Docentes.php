<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Docentes extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [

        'id',

        'nombre',

        'apellido_1',

        'apellido_2',

        'DNI',

        'sexo',

        'num_seg_soc',

        'direccion',

        'codigo_postal',

        'num_cuenta',

        'email',

        'telefono',

        'titulaciones_id',

        'localidades_id'

    ];

    /**
     * 
     * Se establece una relación 1:N con la localidad. 
     * 
     */
    public function Localidades()
    {
        return $this->hasOne(Localidades::class, 'localidades_id');
    }

    /**
     * Se establece una relación 1:N con las jornadas. 
     */
    public function Jornadas()
    {
        return $this->belongsTo(Jornadas::class);
    }

    /**
     * 
     * Se establece una relación 1:N con las titulaciones. 
     * 
     */
    public function Titulaciones()
    {
        return $this->hasOne(Titulaciones::class, 'titulaciones_id');
    }
}
