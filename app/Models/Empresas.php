<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresas extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [

        'id',

        'nombre',

        'denom_comercial',

        'telefono',

        'email',

        'codigo_postal',

        'localidades_id'

    ];

    /**
     * 
     * Se establece una relación 1:N con la localidad. 
     * 
     */
    public function Localidades()
    {
        return $this->hasOne(Localidades::class, 'localidades_id');
    }

    /**
     * 
     * Se establece una relación con la ternaria. 
     * 
     */
    public function Alumnos_Empresas_Jornadas()
    {
        return $this->hasMany(Alumnos_Empresas_Jornadas::class);
    }
}
