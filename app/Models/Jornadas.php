<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jornadas extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [

        'id',

        'fecha_ini',

        'fecha_fin',

        'num_sesiones',

        'num_expediente',

        'entidad_organizadora',

        'nombre_comercial',

        'horario',

        'docentes_id',

        'cursos_id',

        'centros_id',

        'aulas_id'

    ];

    /**
     * 
     * Se establece una relación 1:N con los cursos. 
     * 
     */
    public function Cursos()
    {
        return $this->belongsTo(Cursos::class);
    }

    /**
     * 
     * Se establece una relación 1:N con los docentes. 
     * 
     */
    public function Docentes()
    {
        return $this->hasOne(Docentes::class, 'docentes_id');
    }

    /**
     * 
     * Se establece una relación 1:N con los centros. 
     * 
     */
    public function Centros()
    {
        return $this->hasOne(Centros::class, 'centros_id');
    }

    /**
     * 
     * Se establece una relación 1:N con las sesiones. 
     * 
     */
    public function Sesiones()
    {
        return $this->hasMany(Sesiones::class);
    }

    /**
     * 
     * Se establece una relación con la ternaria. 
     * 
     */
    public function Alumnos_Empresas_Jornadas()
    {
        return $this->hasMany(Alumnos_Empresas_Jornadas::class);
    }

    /**
     * 
     * Se establece una relación 1:N con el aula.  
     * 
     */
    public function Aulas()
    {
        return $this->hasOne(Aulas::class, 'aulas_id');
    }
}
