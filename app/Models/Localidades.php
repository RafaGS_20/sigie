<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Localidades extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [

        'id',

        'localidad'

    ];

    /**
     * Se establecen las relaciones inversas con los modelos vinculados. 
     */
    public function Alumnos()
    {
        return $this->belongsTo(Alumnos::class);
    }

    public function Centros()
    {
        return $this->belongsTo(Centros::class);
    }

    public function Empresas()
    {
        return $this->belongsTo(Empresas::class);
    }

    public function Docentes()
    {
        return $this->belongsTo(Docentes::class);
    }
}
