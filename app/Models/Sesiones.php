<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sesiones extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [

        'id',

        'fecha',

        'jornadas_id'

    ];

    /**
     * 
     * Se establece una relación 1:N con las jornadas. 
     * 
     */
    public function Jornadas()
    {
        return $this->belongsTo(Jornadas::class);
    }
}
