<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Titulaciones extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [

        'id',

        'nombre'
    ];

    /**
     * 
     * Se establece una relación 1:N con los docentes. 
     * 
     */
    public function Docentes()
    {
        return $this->belongsTo(Docentes::class);
    }
}
