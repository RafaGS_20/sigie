<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centros', function (Blueprint $table) {
            $table->id()->onDelete('set null');
            $table->string('nombre');
            $table->string('calle_centro');
            $table->string('num');
            $table->string('codigo_postal', 6);
            $table->string('piso')->nullable();
            $table->string('email');
            $table->string('telefono');
            // Claves foráneas. 
            $table->unsignedBiginteger('localidades_id')->nullable();
            $table->foreign('localidades_id')->references('id')->on('localidades')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centros');
    }
}
