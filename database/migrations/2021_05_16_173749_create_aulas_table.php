<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aulas', function (Blueprint $table) {
            $table->id();
            $table->string('num');
            $table->string('aforo');
            $table->string('equipo');
            $table->string('descripcion');
            // Clave foránea del centro al que pertenece. 
            $table->unsignedBiginteger('centros_id')->nullable();
            $table->foreign('centros_id')->references('id')->on('centros')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aulas');
    }
}
