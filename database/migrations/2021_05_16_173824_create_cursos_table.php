<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->id()->onDelete('set null');
            $table->string('nombre');
            $table->string('horas');
            $table->string('titulacion_requerida');
            $table->string('modalidad');
            $table->string('num_alum_max');
            $table->string('num_alum_min');
            $table->enum('poliza', ['si','no']);
            $table->string('objetivos');
            $table->string('contenidos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
