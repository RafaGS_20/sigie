<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docentes', function (Blueprint $table) {
            $table->id()->onDelete('set null');
            $table->string('nombre');
            $table->string('apellido_1');
            $table->string('apellido_2');
            $table->string('DNI')->unique();
            $table->enum('sexo', ['H', 'M']);
            $table->string('num_seg_soc');
            $table->string('direccion');
            $table->string('codigo_postal', 6);
            $table->string('num_cuenta');
            $table->string('email');
            $table->string('telefono');
            // Clave foránea de la titulación que tiene. 
            $table->unsignedBiginteger('titulaciones_id')->nullable();
            $table->foreign('titulaciones_id')->references('id')->on('titulaciones')->onDelete('set null');
            // Clave foránea de la localidad en la que vive.  
            $table->unsignedBiginteger('localidades_id')->nullable();
            $table->foreign('localidades_id')->references('id')->on('localidades')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docentes');
    }
}
