<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id()->onDelete('set null');
            $table->string('nombre');
            $table->string('denom_comercial');
            $table->string('telefono');
            $table->string('email');
            $table->string('codigo_postal', 6);
            // Clave foránea de la localidad. 
            $table->unsignedBiginteger('localidades_id')->nullable();
            $table->foreign('localidades_id')->references('id')->on('localidades')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}