<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->id()->onDelete('set null');
            $table->string('nombre');
            $table->string('apellido_1');
            $table->string('apellido_2');
            $table->string('DNI');
            $table->enum('sexo', ['H', 'M']);
            $table->enum('discapacidad', ['Si', 'No']); 
            $table->date('fecha_nacimiento'); 
            $table->string('nivel_estudios');
            $table->string('direccion');
            $table->string('codigo_postal', 6);
            $table->string('telefono');
            $table->string('email');
            // Clave foránea de la localidad. 
            $table->unsignedBiginteger('localidades_id')->nullable();
            $table->foreign('localidades_id')->references('id')->on('localidades')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}