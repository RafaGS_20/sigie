<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJornadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jornadas', function (Blueprint $table) {
            $table->id()->onDelete('set null');
            $table->date('fecha_ini');
            $table->date('fecha_fin');
            $table->string('num_sesiones');
            $table->string('num_expediente');
            $table->string('entidad_organizadora');
            $table->string('nombre_comercial');
            $table->string('horario');
            // Clave foránea del docente a impartir.  
            $table->unsignedBiginteger('docentes_id')->nullable();
            $table->foreign('docentes_id')->references('id')->on('docentes')->onDelete('set null');
            // Clave foránea del curso a impartir.  
            $table->unsignedBiginteger('cursos_id')->nullable();
            $table->foreign('cursos_id')->references('id')->on('cursos')->onDelete('set null');
            // Clave foránea del centro en el que se imparte.  
            $table->unsignedBiginteger('centros_id')->nullable();
            $table->foreign('centros_id')->references('id')->on('centros')->onDelete('set null');
            // Clave foránea del aula que se usa del centro. 
            $table->unsignedBiginteger('aulas_id')->nullable();
            $table->foreign('aulas_id')->references('id')->on('aulas')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jornadas');
    }
}
