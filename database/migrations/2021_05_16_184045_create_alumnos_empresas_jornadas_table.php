<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumnosEmpresasJornadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos_empresas_jornadas', function (Blueprint $table) {
            $table->id()->onDelete('set null');
            // Clave foránea del alumno.  
            $table->unsignedBiginteger('alumnos_id')->nullable();
            $table->foreign('alumnos_id')->references('id')->on('alumnos')->onDelete('set null');
            // Clave foránea de la empresa.  
            $table->unsignedBiginteger('empresas_id')->nullable();
            $table->foreign('empresas_id')->references('id')->on('empresas')->onDelete('set null');
            // Clave foránea de la jornada. 
            $table->unsignedBiginteger('jornadas_id')->nullable();
            $table->foreign('jornadas_id')->references('id')->on('jornadas')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos_empresas_jornadas');
    }
}
