<?php

namespace Database\Seeders;

use App\Models\Alumnos;
use Illuminate\Database\Seeder;

class AlumnosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Alumnos::create([

            'nombre' => 'Jhon',

            'apellido_1' => 'Walker',

            'apellido_2' => 'Black',

            'DNI' => '45781236J',

            'sexo' => 'H',

            'discapacidad' => 'No',

            'fecha_nacimiento' => '1820/01/01',

            'nivel_estudios' => 'Bachillerato',

            'direccion' => 'C/Falsa 123',

            'codigo_postal' => '35600',

            'telefono' => '656326598',

            'email' => 'jhon@walker',

            'localidades_id' => '1',

        ]);
    }
}
