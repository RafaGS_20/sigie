<?php

namespace Database\Seeders;

use App\Models\Aulas;
use Illuminate\Database\Seeder;

class AulasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Aulas::create([ 

            'num' => '21',
    
            'aforo' => '15',
    
            'equipo' => 'Ordenadores, proyector',
    
            'descripcion' => 'Aula de informática',
    
            'centros_id' => '1',

        ]);

    }
}
