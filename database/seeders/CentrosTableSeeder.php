<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Centros;

class CentrosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Centros::create([

            'nombre' => 'Majada Marcial',

            'calle_centro' => 'C/Majada marcial',

            'num' => '1',

            'codigo_postal' => '35600',

            'piso' => '1',

            'email' => 'majada@marcial',

            'telefono' => '928568923',

            'localidades_id' => '4'

        ]);
    }
}
