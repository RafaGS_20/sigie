<?php

namespace Database\Seeders;

use App\Models\Cursos;
use Illuminate\Database\Seeder;

class CursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cursos::create([

            'nombre'=> 'Técnico Fitosanitario',

            'horas'=> '30',

            'titulacion_requerida'=> 'ESO',

            'modalidad'=> 'Presencial',

            'num_alum_max'=> '15',

            'num_alum_min'=> '9',

            'poliza'=> 'no',

            'objetivos'=> 'Sacar el curso',

            'contenidos' => 'Muchos'

        ]);
    }
}
