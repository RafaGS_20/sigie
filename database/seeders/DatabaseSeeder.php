<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocalidadesTableSeeder::class);
        $this->call(AlumnosTableSeeder::class);
        $this->call(TitulacionesTableSeeder::class);
        $this->call(CentrosTableSeeder::class);
        $this->call(AulasTableSeeder::class);
        $this->call(CursosTableSeeder::class);
        $this->call(DocentesTableSeeder::class);
        $this->call(EmpresasTableSeeder::class);
        $this->call(JornadasTableSeeder::class);
        $this->call(SesionesTableSeeder::class);
        $this->call(VincularTableSeeder::class);
    }
}
