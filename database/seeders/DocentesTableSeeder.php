<?php

namespace Database\Seeders;

use App\Models\Docentes;
use Illuminate\Database\Seeder;

class DocentesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Docentes::create([

            'nombre' => 'Margaret' ,
    
            'apellido_1' => 'Tatcher' ,
    
            'apellido_2' => 'Rudolf' ,
    
            'DNI' => '25896314G' ,
    
            'sexo' => 'M' ,
    
            'num_seg_soc' => '45789632Q' ,
    
            'direccion' => 'C/Falsa 23' ,
    
            'codigo_postal' => '35600' ,
    
            'num_cuenta' => 'ES7845963214569' ,
    
            'email' => 'Margaret@rudolf' ,
    
            'telefono' => '652369874' ,
    
            'titulaciones_id' => '1' ,
    
            'localidades_id' => '5' 

        ]);
    }
}
