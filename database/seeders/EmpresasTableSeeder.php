<?php

namespace Database\Seeders;

use App\Models\Empresas;
use Illuminate\Database\Seeder;

class EmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empresas::create([

            'nombre' => 'e2Formacion' ,

            'denom_comercial' => 'e2formacion s.l' ,
    
            'telefono' => '928568923' ,
    
            'email' => 'e2@formacion' ,
    
            'codigo_postal' => '35600' ,
    
            'localidades_id' => '4' 

        ]);
    }
}
