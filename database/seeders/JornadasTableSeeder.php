<?php

namespace Database\Seeders;

use App\Models\Jornadas;
use Illuminate\Database\Seeder;

class JornadasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jornadas::create([

            'fecha_ini' => '2021/05/17',

            'fecha_fin' => '2021/05/26',

            'num_sesiones' => '4',

            'num_expediente' => '1254',

            'entidad_organizadora' => '45721685',

            'nombre_comercial' => 'e2formacion',

            'horario' => 'tarde',

            'docentes_id' => '1',

            'cursos_id' => '1',

            'centros_id' => '1', 

            'aulas_id' => '1',

        ]);
    }
}
