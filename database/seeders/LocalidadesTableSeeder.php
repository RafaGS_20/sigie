<?php

namespace Database\Seeders;

use App\Models\Localidades;
use Illuminate\Database\Seeder;

class LocalidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Fuerteventura
        Localidades::create(['localidad' => 'Antigua']);
        Localidades::create(['localidad' => 'Betancuria']);
        Localidades::create(['localidad' => 'La Oliva']);
        Localidades::create(['localidad' => 'Pájara']);
        Localidades::create(['localidad' => 'Puerto del Rosario']);
        Localidades::create(['localidad' => 'Tuineje']);
        Localidades::create(['localidad' => 'Ajuy']);
        Localidades::create(['localidad' => 'Caleta de Fueste']);
        Localidades::create(['localidad' => 'Corralejo']);
        Localidades::create(['localidad' => 'El Cotillo']);
        Localidades::create(['localidad' => 'Gran Tarajal']);
        Localidades::create(['localidad' => 'Guisguey']);
        Localidades::create(['localidad' => 'Lajares']);
        Localidades::create(['localidad' => 'Morro Jable']);
        Localidades::create(['localidad' => 'Tindaya']);
        Localidades::create(['localidad' => 'Villaverde']);

    }
}
