<?php

namespace Database\Seeders;

use App\Models\Sesiones;
use Illuminate\Database\Seeder;

class SesionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Sesiones::create([

            'fecha' => '2021-05-17',

            'jornadas_id' => '1'

        ]);

        Sesiones::create([

            'fecha' => '2021-05-19',

            'jornadas_id' => '1'

        ]);

        Sesiones::create([

            'fecha' => '2021-05-21',

            'jornadas_id' => '1'

        ]);

        Sesiones::create([

            'fecha' => '2021-05-26',

            'jornadas_id' => '1'

        ]);
    }
}
