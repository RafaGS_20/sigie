<?php

namespace Database\Seeders;

use App\Models\Titulaciones;
use Illuminate\Database\Seeder;

class TitulacionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Titulaciones::create([

            'nombre' => 'Técnico superior en enseñanza de cosas'

        ]);
    }
}
