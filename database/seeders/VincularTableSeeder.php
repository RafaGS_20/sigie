<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AlumnosEmpresasJornadas;

class VincularTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        AlumnosEmpresasJornadas::create([

            'alumnos_id' => '1',

            'empresas_id' => '1',

            'jornadas_id' => '1'

        ]);
    }
}
