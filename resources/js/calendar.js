import { Calendar } from '@fullcalendar/core';
import 'bootstrap/dist/css/bootstrap.css';
import dayGridPlugin from '@fullcalendar/daygrid'; // Botón que te lleva al día. 
import timeGridPlugin from '@fullcalendar/timegrid'; // Semana. 
import listPlugin from '@fullcalendar/list'; // Agenda. 
import esLocale from '@fullcalendar/core/locales/es'; // En español.
import interactionPlugin from '@fullcalendar/interaction'; // Plugins para interacciones. 
import momentPlugin from '@fullcalendar/moment'; // Moment para el calendario. 
import bootstrapPlugin from '@fullcalendar/bootstrap';

document.addEventListener('DOMContentLoaded', function () {
  const calendarEl = document.getElementById("calendar");

  var calendar = new Calendar(calendarEl, {
    themeSystem: 'bootstrap',
    customButtons: {
      nuevaJornada: {
        text: 'Añadir jornada',
        click: function () {
          window.location.href = '/intruccionesjornadas';
        }
      }
    },
    headerToolbar: {
      left: 'prev,next today nuevaJornada',
      center: 'title',
      right: 'dayGridMonth listWeek'
    },
    locale: esLocale,
    plugins: [dayGridPlugin, listPlugin, momentPlugin, bootstrapPlugin],
    initialView: 'dayGridMonth',
    events: '/api/fullcalendar',
    /** Con esta función al seleccionar una jornada nos lleva a su página concreta, con toda su información. 
     *  Funciona tanto desde el calendario mensual como desde la agenda semanal.  */
    eventClick: function (info) {
      window.location.href = '/Jornadas/' + info.event.id;
    }
  });

  calendar.render();
});

