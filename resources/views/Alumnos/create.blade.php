@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Alta individual de un alumno. </h2>
        </div>
    </div>
</div>

<br>

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<form action="{{ route('Alumnos.store') }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-xs-3 col-sm-3 col-md-3">
            <div class="form-group">
                <strong> Nombre:</strong>
                <input type="text" name="nombre" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <div class="form-group">
                <strong> Primer apellido:</strong>
                <input type="text" name="apellido_1" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <div class="form-group">
                <strong> Segundo apellido:</strong>
                <input type="text" name="apellido_2" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <div class="form-group">
                <strong> DNI:</strong>
                <input type="text" name="DNI" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <label for="cars">Sexo:</label>
            <select name="sexo" id="sexo">
                <option value="H"> Hombre </option>
                <option value="M"> Mujer </option>
            </select>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <label for="cars">Discapacidad:</label>
            <select name="discapacidad" id="discapacidad">
                <option value="Sí"> Sí </option>
                <option value="No"> No </option>
            </select>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <div class="form-group">
                <strong> Fecha de nacimiento:</strong>
                <input type="date" name="fecha_nacimiento" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <div class="form-group">
                <strong> Nivel de estudios:</strong>
                <input type="text" name="nivel_estudios" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <div class="form-group">
                <strong> Dirección:</strong>
                <input type="text" name="direccion" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <div class="form-group">
                <strong> Código postal:</strong>
                <input type="text" name="codigo_postal" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong> Teléfono:</strong>
                <input type="text" name="telefono" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong> Email:</strong>
                <input type="text" name="email" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <label for="cars">Elija una localidad:</label>
            <select name="localidades_id" id="localidades_id">
                @foreach ($localidades as $localidad)
                <option value="{{ $localidad->id }}"> {{ $localidad->localidad }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary"> Dar de alta </button>
        </div>
    </div>


</form>

@endsection