@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Modificando el alumno {{ $alumno->nombre }} {{ $alumno->apellido_1 }} {{ $alumno->apellido_2 }}. </h2>
        </div>
    </div>
</div>

<div class="row" style="margin-top:40px">
    <form method="POST" enctype="multipart/form-data" action="{{url('Alumnos/'.$alumno->id)}}" style="display:inline">
        {{-- TODO: Abrir el formulario e indicar el método POST --}}
        @method('PUT')
        {{-- TODO: Protección contra CSRF --}}
        @csrf

        <div class="row">

            <div class="col-xs-3 col-sm-3 col-md-3">
                <div class="form-group">
                    <strong> Nombre:</strong>
                    <input type="text" name="nombre" class="form-control" value="{{ $alumno->nombre }}">
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                <div class="form-group">
                    <strong> Primer apellido:</strong>
                    <input type="text" name="apellido_1" class="form-control" value="{{ $alumno->apellido_1 }}">
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                <div class="form-group">
                    <strong> Segundo apellido:</strong>
                    <input type="text" name="apellido_2" class="form-control" value="{{ $alumno->apellido_2 }}">
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                <div class="form-group">
                    <strong> DNI:</strong>
                    <input type="text" name="DNI" class="form-control" value="{{ $alumno->DNI }}">
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <label for="cars">Sexo:</label>
                <select name="sexo" id="sexo">
                    <option value="H"> Hombre </option>
                    <option value="M"> Mujer </option>
                </select>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <label for="cars">Discapacidad:</label>
                <select name="discapacidad" id="discapacidad">
                    <option value="Sí"> Sí </option>
                    <option value="No"> No </option>
                </select>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                <div class="form-group">
                    <strong> Fecha de nacimiento:</strong>
                    <input type="date" name="fecha_nacimiento" class="form-control" value="{{ $alumno->fecha_nacimiento }}">
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                <div class="form-group">
                    <strong> Nivel de estudios:</strong>
                    <input type="text" name="nivel_estudios" class="form-control" value="{{ $alumno->nivel_estudios }}">
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                <div class="form-group">
                    <strong> Dirección:</strong>
                    <input type="text" name="direccion" class="form-control" value="{{ $alumno->direccion }}">
                </div>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                <div class="form-group">
                    <strong> Código postal:</strong>
                    <input type="text" name="codigo_postal" class="form-control" value="{{ $alumno->codigo_postal }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong> Teléfono:</strong>
                    <input type="text" name="telefono" class="form-control" value="{{ $alumno->telefono }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong> Email:</strong>
                    <input type="text" name="email" class="form-control" value="{{ $alumno->email }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <label for="cars">Elija una localidad:</label>
                <select name="localidades_id" id="localidades_id">
                    @foreach ($localidades as $localidad)
                    <option value="{{ $localidad->id }}"> {{ $localidad->localidad }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary"> Realizar la modificación </button>
            </div>
    </form>

</div>

<br>

@stop