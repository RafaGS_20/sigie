@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Alumnos </h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('Alumnos.create') }}"> Alta de alumno individual </a> <!-- Creación individual. -->
            <a class="btn btn-success" href="{{ url('/Alumnosmasivo') }}"> Alta de alumno masiva </a> <!-- Dar de alta de forma masiva con el import. -->
        </div>
    </div>
</div>

<br>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="row">
    <table class="table table-striped table-bordered">
        <tr>
            <th> Nombre completo </th>
            <th> DNI </th>
            <th> Sexo </th>
            <th> Discapacidad </th>
            <th> Fecha de nacimiento </th>
            <th> Nivel de estudios </th>
            <th> Dirección </th>
            <th> Localidad </th>
            <th> Teléfono </th>
            <th> Email </th>
            <th> Opciones </th>
        </tr>
        @foreach ($alumnos as $alumno)
        <tr>
            <td> {{$alumno->nombre}} {{$alumno->apellido_1}} {{$alumno->apellido_2}} </td>
            <td> {{$alumno->DNI}} </td>
            <td> {{$alumno->sexo}} </td>
            <td> {{$alumno->discapacidad}} </td>
            <td> {{$alumno->fecha_nacimiento}} </td>
            <td> {{$alumno->nivel_estudios}} </td>
            <td> {{$alumno->direccion}} </td>
            <td> <?php if ($alumno->localidades_id != 0) {
                ?> {{$localidades[$alumno->localidades_id -1]->localidad}} <?php } else {
                ?> No asignado <?php } ?></td>
            <td> {{$alumno->telefono}} </td>
            <td> {{$alumno->email}} </td>
            <td>

                <form method="POST" action="{{ url('/Alumnos/' .$alumno->id) }}" style="display:inline">

                    <a class="btn btn-success" href="{{ route('Alumnos.edit',$alumno->id) }}"> Editar </a>

                    @method('DELETE')

                    @csrf

                    @include('Alumnos.destroy')

                    <a href="" data-target="#modal-delete-{{$alumno->id}}" data-toggle="modal" class="btn btn-danger">Eliminar</a>

                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div>

@endsection