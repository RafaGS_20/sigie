@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Modificando el aula número {{ $aula->num }}. </h2>
        </div>
    </div>
</div>

<div class="row" style="margin-top:40px">
    <form method="POST" enctype="multipart/form-data" action="{{url('Aulas/'.$aula->id)}}" style="display:inline">
        {{-- TODO: Abrir el formulario e indicar el método POST --}}
        @method('PUT')
        {{-- TODO: Protección contra CSRF --}}
        @csrf

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Número del aula:</strong>
                    <input type="text" name="num" class="form-control" value="{{ $aula->num }}" >
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong> Aforo:</strong>
                    <input type="text" name="aforo" class="form-control" value="{{ $aula->aforo }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong> Equipo:</strong>
                    <input type="text" name="equipo" class="form-control" value="{{ $aula->equipo }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong> Descripción:</strong>
                    <input type="text" name="descripcion" class="form-control" value="{{ $aula->descripcion }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <label for="cars">Elija el centro al que pertenece:</label>
                <select name="centros_id" id="centros_id">
                    @foreach ($centros as $centro)
                    <option value="{{ $centro->id }}"> {{ $centro->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary"> Realizar la modificación </button>
            </div>
    </form>

</div>

<br>

@stop