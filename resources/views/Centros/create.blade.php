@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Dar de alta un nuevo centro. </h2>
        </div>
    </div>
</div>

<br>

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<form action="{{ route('Centros.store') }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre del centro:</strong>
                <input type="text" name="nombre" class="form-control" placeholder="Nombre del nuevo centro">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong> Calle:</strong>
                <input type="text" name="calle_centro" class="form-control" placeholder="Calle del centro">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong> Número de la calle:</strong>
                <input type="text" name="num" class="form-control" placeholder="Núm">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong> Código postal:</strong>
                <input type="text" name="codigo_postal" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong> Planta:</strong>
                <input type="text" name="piso" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong> Email:</strong>
                <input type="text" name="email" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong> Teléfono de contacto:</strong>
                <input type="text" name="telefono" class="form-control" placeholder="Introduzca teléfono fijo o móvil">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <label for="cars">Elija una localidad:</label>
                <select name="localidades_id" id="localidades_id">
                    @foreach ($localidades as $localidad)
                    <option value="{{ $localidad->id }}"> {{ $localidad->localidad }}</option>
                    @endforeach
                </select>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary"> Dar de alta </button>
        </div>
    </div>


</form>

@endsection