@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Modificando el centro {{ $centro->nombre }}. </h2>
        </div>
    </div>
</div>

<div class="row" style="margin-top:40px">
    <form method="POST" enctype="multipart/form-data" action="{{url('Centros/'.$centro->id)}}" style="display:inline">
        {{-- TODO: Abrir el formulario e indicar el método POST --}}
        @method('PUT')
        {{-- TODO: Protección contra CSRF --}}
        @csrf

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nombre del centro:</strong>
                    <input type="text" name="nombre" class="form-control" value="{{ $centro->nombre }}" >
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong> Calle:</strong>
                    <input type="text" name="calle_centro" class="form-control" value="{{ $centro->calle_centro }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong> Número de la calle:</strong>
                    <input type="text" name="num" class="form-control" value="{{ $centro->num }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong> Código postal:</strong>
                    <input type="text" name="codigo_postal" class="form-control" value="{{ $centro->codigo_postal }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong> Planta:</strong>
                    <input type="text" name="piso" class="form-control" value="{{ $centro->piso }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong> Email:</strong>
                    <input type="text" name="email" class="form-control" value="{{ $centro->email }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong> Teléfono de contacto:</strong>
                    <input type="text" name="telefono" class="form-control" value="{{ $centro->telefono }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <label for="cars">Elija una localidad:</label>
                <select name="localidades_id" id="localidades_id">
                    @foreach ($localidades as $localidad)
                    <option value="{{ $localidad->id }}"> {{ $localidad->localidad }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary"> Realizar la modificación </button>
            </div>
    </form>

</div>

<br>

@stop