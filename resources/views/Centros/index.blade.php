@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Centros </h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('Centros.create') }}"> Dar de alta un nuevo centro </a>
        </div>
    </div>
</div>

<br>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="row">
    <table class="table table-striped table-bordered">
        <tr>
            <th> Nombre </th>
            <th> Dirección </th>
            <th> Código postal </th>
            <th> Localidad </th>
            <th> Email </th>
            <th> Teléfono </th>
            <th> Opciones </th>
        </tr>
        @foreach ($centros as $centro)
        <tr>
            <td> {{ $centro->nombre }} </td>
            <td> {{ $centro->calle_centro }}, número {{ $centro->num }}, piso {{ $centro->piso }} </td>
            <td> {{ $centro->codigo_postal }} </td>
            <td> {{$localidades[$centro->localidades_id - 1]->localidad}} </td>
            <td> {{ $centro->email }} </td>
            <td> {{ $centro->telefono }} </td>
            <td>

                <form method="POST" action=" {{  url( '/Centros/' .$centro->id ) }}" style="display:inline">

                    <a class="btn btn-success" href="{{ route('Centros.edit',$centro->id) }}"> Editar </a>

                    @method('DELETE')

                    @csrf

                    @include('Centros.destroy')

                    <a href="" data-target="#modal-delete-{{$centro->id}}" data-toggle="modal" class="btn btn-danger">Eliminar</a>

                    <a class="btn btn-info" href="{{ route('Centros.show',$centro->id) }}"> Ver detalles </a>

                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div>

@endsection