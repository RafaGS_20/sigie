@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Centro {{ $centro->nombre}}. </h2>
        </div>
    </div>
</div>

<div class="row">
    <table class="table table-striped table-bordered">
        <tr>
            <th> Nombre </th>
            <th> Dirección </th>
            <th> Código postal </th>
            <th> Localidad </th>
            <th> Email </th>
            <th> Teléfono </th>
        </tr>
        <tr>
            <td> {{ $centro->nombre }} </td>
            <td> {{ $centro->calle_centro }}, número {{ $centro->num }}, piso {{ $centro->piso }} </td>
            <td> {{ $centro->codigo_postal }} </td>
            <td> {{$localidades[$centro->localidades_id - 1]->localidad}} </td>
            <td> {{ $centro->email }} </td>
            <td> {{ $centro->telefono }} </td>
        </tr>
    </table>
</div>

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <a class="btn btn-success" href="{{ route('Aulas.create') }}"> Añadir aulas </a>
        </div>
    </div>
</div>

<br>

<div class="row">
    <table class="table table-striped table-bordered">
        <tr>
            <th> Número </th>
            <th> Aforo </th>
            <th> Equipo </th>
            <th> Descripcion </th>
            <th> Opciones </th>
        </tr>
        @foreach ($aulas as $aula)
        <tr>
            <td> {{$aula->num}} </td>
            <td> {{$aula->aforo}}</td>
            <td> {{$aula->equipo}}</td>
            <td> {{$aula->descripcion}}</td>
            <td>

                <form method="POST" action=" {{  url( '/Aulas/' .$aula->id ) }}" style="display:inline">

                    <a class="btn btn-success" href="{{ route('Aulas.edit',$aula->id) }}"> Editar </a>

                    @method('DELETE')

                    @csrf

                    @include('Aulas.destroy')

                    <a href="" data-target="#modal-delete-{{$aula->id}}" data-toggle="modal" class="btn btn-danger">Eliminar</a>

                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div>
@endsection