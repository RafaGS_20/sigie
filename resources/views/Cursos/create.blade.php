@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Dar de alta a un nuevo curso. </h2>
        </div>
    </div>
</div>

<br>

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<form action="{{ route('Cursos.store') }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Nombre del curso:</strong>
                <input type="text" name="nombre" class="form-control" placeholder="Nombre del curso">
            </div>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2">
            <div class="form-group">
                <strong>Horas:</strong>
                <input type="text" name="horas" class="form-control" placeholder="Horas en número">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong>Titulación requerida:</strong>
                <input type="text" name="titulacion_requerida" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong>Modalidad:</strong>
                <input type="text" name="modalidad" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2">
            <div class="form-group">
                <strong>Alumnos mínimos:</strong>
                <input type="text" name="num_alum_min" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2">
            <div class="form-group">
                <strong>Alumnos máximos:</strong>
                <input type="text" name="num_alum_max" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <label for="cars">Necesita poliza extra:</label>
            <select name="poliza" id="poliza">
                <option value="no"> No </option>
                <option value="si"> Sí </option>
            </select>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <p>Objetivos del curso:</p>
            <textarea name="objetivos" rows="10" cols="80" placeholder="Objetivos del curso"> </textarea>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <p>Contenidos del curso:</p>
            <textarea name="contenidos" rows="10" cols="80" placeholder="Contenidos del curso"> </textarea>
        </div>
        <br>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary"> Dar de alta </button>
        </div>
    </div>


</form>

@endsection