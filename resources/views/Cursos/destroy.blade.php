<div class="modal" tabindex="-1" role="dialog" id="modal-delete-{{$curso->id}}">
    <form method="POST" action="{{url('/Cursos/delete').'/'.$curso->id}}" style="display:inline">
        @method('DELETE')
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar registro</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p> El registro puede tener jornadas asociadas y se perderá información sobre ellas ¿Está seguro de querer borrar este curso? </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </form>
</div>