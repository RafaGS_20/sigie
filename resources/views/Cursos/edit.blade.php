@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Modificando datos del docente curso {{$curso->nombre}}. </h2>
        </div>
    </div>
</div>

<div class="row" style="margin-top:40px">
    <form method="POST" enctype="multipart/form-data" action="{{url('Cursos/'.$curso->id)}}" style="display:inline">
        {{-- TODO: Abrir el formulario e indicar el método POST --}}
        @method('PUT')
        {{-- TODO: Protección contra CSRF --}}
        @csrf

        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Nombre del curso:</strong>
                    <input type="text" name="nombre" class="form-control" value="{{$curso->nombre}}">
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2">
                <div class="form-group">
                    <strong>Horas:</strong>
                    <input type="text" name="horas" class="form-control" value="{{$curso->horas}}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong>Titulación requerida:</strong>
                    <input type="text" name="titulacion_requerida" class="form-control" value="{{$curso->titulacion_requerida}}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong>Modalidad:</strong>
                    <input type="text" name="modalidad" class="form-control" value="{{$curso->modalidad}}">
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2">
                <div class="form-group">
                    <strong>Alumnos mínimos:</strong>
                    <input type="text" name="num_alum_min" class="form-control" value="{{$curso->num_alum_min}}">
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2">
                <div class="form-group">
                    <strong>Alumnos máximos:</strong>
                    <input type="text" name="num_alum_max" class="form-control" value="{{$curso->num_alum_max}}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <label for="cars">Necesita poliza extra:</label>
                <select name="poliza" id="poliza">
                    <option value="no"> No </option>
                    <option value="si"> Sí </option>
                </select>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <p>Objetivos del curso:</p>
                <textarea name="objetivos" rows="10" cols="80" placeholder="Objetivos del curso"> {{$curso->objetivos}}</textarea>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <p>Contenidos del curso:</p>
                <textarea name="contenidos" rows="10" cols="80" placeholder="Contenidos del curso"> {{$curso->contenidos}}</textarea>
            </div>
            <br>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary"> Realizar la modificación </button>
        </div>
    </form>

</div>

<br>

@stop