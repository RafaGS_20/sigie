@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Cursos </h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('Cursos.create') }}"> Dar de alta un nuevo curso </a>
        </div>
    </div>
</div>

<br>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="row">
    <table class="table table-striped table-bordered">
        <tr>
            <th> Nombre </th>
            <th> Horas </th>
            <th> Titulación requerida </th>
            <th> Modalidad </th>
            <th> Alumnos </th>
            <th> Poliza </th>
            <th> Objetivos </th>
            <th> Contenidos </th>
            <th> Opciones </th>
        </tr>
        @foreach ($cursos as $curso)
        <tr>
            <td> {{ $curso->nombre }} </td>
            <td> {{$curso->horas}}  </td>
            <td> {{$curso->titulacion_requerida}} </td>
            <td> {{$curso->modalidad}} </td>
            <td> Entre {{$curso->num_alum_min}} y {{$curso->num_alum_max}}</td>
            <td> {{$curso->poliza}} </td>
            <td> {{$curso->objetivos}} </td>
            <td> {{$curso->contenidos}} </td>
            <td>

                <form method="POST" action=" {{  url( '/Cursos/' .$curso->id ) }}" style="display:inline">

                    <a class="btn btn-success" href="{{ route('Cursos.edit',$curso->id) }}"> Editar </a>

                    @method('DELETE')

                    @csrf

                    @include('Cursos.destroy')

                    <a href="" data-target="#modal-delete-{{$curso->id}}" data-toggle="modal" class="btn btn-danger">Eliminar</a>

                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div>

@endsection