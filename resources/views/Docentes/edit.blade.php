@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Modificando datos del docente {{ $docente->nombre }} {{ $docente->apellido_1 }} {{ $docente->apellido_2 }}. </h2>
        </div>
    </div>
</div>

<div class="row" style="margin-top:40px">
    <form method="POST" enctype="multipart/form-data" action="{{url('Docentes/'.$docente->id)}}" style="display:inline">
        {{-- TODO: Abrir el formulario e indicar el método POST --}}
        @method('PUT')
        {{-- TODO: Protección contra CSRF --}}
        @csrf

        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong>Nombre del docente:</strong>
                    <input type="text" name="nombre" class="form-control" value="{{ $docente->nombre }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong>Primer apellido:</strong>
                    <input type="text" name="apellido_1" class="form-control" value="{{ $docente->apellido_1 }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong>Segundo apellido:</strong>
                    <input type="text" name="apellido_2" class="form-control" value="{{ $docente->apellido_2 }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong>DNI:</strong>
                    <input type="text" name="DNI" class="form-control" value="{{ $docente->DNI }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <label for="cars">Sexo:</label>
                <select name="sexo" id="sexo">
                    <option value="H"> Hombre </option>
                    <option value="M"> Mujer </option>
                </select>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong>Número de la seguridad social:</strong>
                    <input type="text" name="num_seg_soc" class="form-control" value="{{ $docente->num_seg_soc }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8">
                <div class="form-group">
                    <strong> Dirección:</strong>
                    <input type="text" name="direccion" class="form-control" value="{{ $docente->direccion }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong> Código postal:</strong>
                    <input type="text" name="codigo_postal" class="form-control" value="{{ $docente->codigo_postal }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong>Número de cuenta:</strong>
                    <input type="text" name="num_cuenta" class="form-control" value="{{ $docente->num_cuenta }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong> Email:</strong>
                    <input type="text" name="email" class="form-control" value="{{ $docente->email }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong> Teléfono de contacto:</strong>
                    <input type="text" name="telefono" class="form-control" value="{{ $docente->telefono }}">
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <label for="cars">Elija una titulación:</label>
                <select name="titulaciones_id" id="titulaciones_id">
                    @foreach ($titulaciones as $titulacion)
                    <option value="{{ $titulacion->id }}"> {{ $titulacion->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <label for="cars">Elija una localidad:</label>
                <select name="localidades_id" id="localidades_id">
                    @foreach ($localidades as $localidad)
                    <option value="{{ $localidad->id }}"> {{ $localidad->localidad }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary"> Realizar la modificación </button>
            </div>
    </form>

</div>

<br>

@stop