@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Docentes </h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('Docentes.create') }}"> Dar de alta a un nuevo docente </a>
            <a class="btn btn-success" href="{{ route('Titulaciones.create') }}"> Dar de alta una nueva titulación </a>
        </div>
    </div>
</div>

<br>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="row">
    <table class="table table-striped table-bordered">
        <tr>
            <th> Nombre </th>
            <th> DNI </th>
            <th> Titulación </th>
            <th> Dirección </th>
            <th> Código postal </th>
            <th> Localidad </th>
            <th> Email </th>
            <th> Teléfono </th>
            <th> Opciones </th>
        </tr>
        @foreach ($docentes as $docente)
        <tr>
            <td> {{ $docente->nombre }} {{$docente->apellido_1}} {{$docente->apellido_2}}</td>
            <td> {{$docente->DNI}}  </td>
            <td> {{$titulaciones[$docente->titulaciones_id -1]->nombre}} </td>
            <td> {{$docente->direccion}} </td>
            <td> {{$docente->codigo_postal}} </td>
            <td> {{$localidades[$docente->localidades_id - 1]->localidad}} </td>
            <td> {{$docente->email}} </td>
            <td> {{$docente->telefono}} </td>
            <td>

                <form method="POST" action=" {{  url( '/Docentes/' .$docente->id ) }}" style="display:inline">

                    <a class="btn btn-success" href="{{ route('Docentes.edit',$docente->id) }}"> Editar </a>

                    @method('DELETE')

                    @csrf

                    @include('Docentes.destroy')

                    <a href="" data-target="#modal-delete-{{$docente->id}}" data-toggle="modal" class="btn btn-danger">Eliminar</a>

                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div>

@endsection