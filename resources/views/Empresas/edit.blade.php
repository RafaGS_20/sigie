@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Modificando datos de la empresa {{ $empresa->nombre }}. </h2>
        </div>
    </div>
</div>

<br>


<div class="row" style="margin-top:40px">
<form method="POST" enctype="multipart/form-data" action="{{url('Empresas/'.$empresa->id)}}" style="display:inline">
{{-- TODO: Abrir el formulario e indicar el método POST --}}
        @method('PUT')
        {{-- TODO: Protección contra CSRF --}}
        @csrf
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Nombre de la empresa:</strong>
                <input type="text" name="nombre" class="form-control" value="{{ $empresa->nombre }}">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Denominación comercial:</strong>
                <input type="text" name="denom_comercial" class="form-control" value="{{ $empresa->denom_comercial }}">
            </div>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2">
            <div class="form-group">
                <strong>Teléfono:</strong>
                <input type="text" name="telefono" class="form-control" value="{{ $empresa->telefono }}">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="text" name="email" class="form-control" value="{{ $empresa->email }}">
            </div>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2">
            <div class="form-group">
                <strong>Código postal:</strong>
                <input type="text" name="codigo_postal" class="form-control" value="{{ $empresa->codigo_postal}}">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <label for="cars">Elija una localidad:</label>
            <select name="localidades_id" id="localidades_id">
                @foreach ($localidades as $localidad)
                <option value="{{ $localidad->id }}"> {{ $localidad->localidad }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Realizar la modificación </button>
        </div>
    </div>


</form>

@endsection