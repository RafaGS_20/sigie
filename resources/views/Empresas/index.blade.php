@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Empresas </h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('Empresas.create') }}"> Dar de alta a una nueva empresa </a>
        </div>
    </div>
</div>

<br>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="row">
    <table class="table table-striped table-bordered">
        <tr>
            <th> Nombre </th>
            <th> Denominación comercial </th>
            <th> Código postal </th>
            <th> Localidad </th>
            <th> Email </th>
            <th> Teléfono </th>
            <th> Opciones </th>
        </tr>
        @foreach ($empresas as $empresa)
        <tr>
            <td> {{ $empresa->nombre }} </td>
            <td> {{$empresa->denom_comercial}}  </td>
            <td> {{$empresa->codigo_postal}} </td>
            <td> {{$localidades[$empresa->localidades_id - 1]->localidad}} </td>
            <td> {{$empresa->email}} </td>
            <td> {{$empresa->telefono}} </td>
            <td>

                <form method="POST" action=" {{  url( '/Empresas/' .$empresa->id ) }}" style="display:inline">

                    <a class="btn btn-success" href="{{ route('Empresas.edit',$empresa->id) }}"> Editar </a>

                    @method('DELETE')

                    @csrf

                    @include('Empresas.destroy')

                    <a href="" data-target="#modal-delete-{{$empresa->id}}" data-toggle="modal" class="btn btn-danger">Eliminar</a>

                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div>

@endsection