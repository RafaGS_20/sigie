@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Registrar una nueva jornada. </h2>
        </div>
    </div>
</div>

<br>

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<form action="{{ route('Jornadas.store') }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong> Fecha de inicio:</strong>
                <input type="date" name="fecha_ini" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong> Fecha de fin:</strong>
                <input type="date" name="fecha_fin" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong> Número de sesiones:</strong>
                <input type="text" name="num_sesiones" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <div class="form-group">
                <strong> Número de expediente:</strong>
                <input type="text" name="num_expediente" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <div class="form-group">
                <strong> Entidad organizadora:</strong>
                <input type="text" name="entidad_organizadora" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <div class="form-group">
                <strong> Nombre comercial:</strong>
                <input type="text" name="nombre_comercial" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <div class="form-group">
                <strong> Horario:</strong>
                <input type="text" name="horario" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <label for="cars"> Docente:</label>
            <select name="docentes_id" id="docentes_id">
                <option value=""></option>
                @foreach ($docentes as $docente)
                <option value="{{ $docente->id }}"> {{ $docente->nombre }} {{ $docente->apellido_1 }} {{ $docente->apellido_2 }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <label for="cars"> Curso:</label>
            <select name="cursos_id" id="cursos_id">
                <option value=""></option>
                @foreach ($cursos as $curso)
                <option value="{{ $curso->id }}"> {{ $curso->nombre }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <label for="cars"> Centro:</label>
            <select name="centros_id" id="centros_id">
                <option value=""></option>
                @foreach ($centros as $centro)
                <option value="{{ $centro->id }}"> {{ $centro->nombre }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <label for="cars"> Aula:</label>
            <select name="aulas_id" id="aulas_id">
            </select>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary"> Dar de alta </button>
        </div>
    </div>

</form>

<script>
    $('#centros_id').on('change', function(e) {
        console.log(e);
        var centro = e.target.value;

        $.get('select_aulas/' + centro, function(data) {

            $('#aulas').empty();

            $.each(data, function(fetch, aulas) {
                console.log(data);
                for (i = 0; i < aulas.length; i++) {
                    $('#aulas_id').append('<option value="' + aulas[i].id + '">' + aulas[i].num + '</option>');
                }
            })
        })
    });
</script>

@endsection