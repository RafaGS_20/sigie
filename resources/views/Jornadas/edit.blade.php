@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Modificando jornada. </h2>
        </div>
    </div>
</div>

<div class="row" style="margin-top:40px">
    <form method="POST" enctype="multipart/form-data" action="{{url('Jornadas/'.$jornada->id)}}" style="display:inline">
        {{-- TODO: Abrir el formulario e indicar el método POST --}}
        @method('PUT')
        {{-- TODO: Protección contra CSRF --}}
        @csrf

        <div class="row">
            <div class="col-xs-2 col-sm-2 col-md-2">
                <div class="form-group">
                    <strong> Fecha de inicio:</strong>
                    <input type="date" name="fecha_ini" class="form-control" value="{{ $jornada->fecha_ini }}">
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2">
                <div class="form-group">
                    <strong> Fecha de fin:</strong>
                    <input type="date" name="fecha_fin" class="form-control" value="{{ $jornada->fecha_fin }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong> Número de sesiones:</strong>
                    <input type="text" name="num_sesiones" class="form-control" value="{{ $jornada->num_sesiones }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong> Número de expediente:</strong>
                    <input type="text" name="num_expediente" class="form-control" value="{{ $jornada->num_expediente }}">
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong> Entidad organizadora:</strong>
                    <input type="text" name="entidad_organizadora" class="form-control" value="{{ $jornada->entidad_organizadora }}">
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong> Nombre comercial:</strong>
                    <input type="text" name="nombre_comercial" class="form-control" value="{{ $jornada->nombre_comercial }}">
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2">
                <div class="form-group">
                    <strong> Horario:</strong>
                    <input type="text" name="horario" class="form-control" value="{{ $jornada->horario }}">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <label for="cars">Docente:</label>
                <select name="docentes_id" id="docentes_id">
                    @foreach ($docentes as $docente)
                    <option value="{{ $docente->id }}"> {{ $docente->nombre }} {{ $docente->apellido_1}} {{ $docente->apellido_2 }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <label for="cars">Curso relacionado:</label>
                <select name="cursos_id" id="cursos_id">
                    @foreach ($cursos as $curso)
                    <option value="{{ $curso->id }}"> {{ $curso->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2">
                <label for="cars">Centro seleccionado:</label>
                <select name="centros_id" id="centros_id">
                    @foreach ($centros as $centro)
                    <option value="{{ $centro->id }}"> {{ $centro->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary"> Realizar la modificación </button>
            </div>
    </form>

</div>

<br>

@stop