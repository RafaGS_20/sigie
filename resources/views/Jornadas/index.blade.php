@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Jornadas </h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ ('/intruccionesjornadas') }}"> Crear una nueva jornada </a> <!-- Redirección al mecanismo de creación de jornadas. -->
        </div>
    </div>
</div>

<br>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="row">
    <table class="table table-striped table-bordered">
        <tr>
            <th> Curso relacionado </th>
            <th> Inicio </th>
            <th> Fin </th>
            <th> Número de sesiones </th>
            <th> Número de expediente </th>
            <th> Horario </th>
            <th> Docente </th>
            <th> Centro </th>
            <th> Aula </th>
            <th> Opciones </th>
        </tr>
        @foreach ($jornadas as $jornada)
        <tr>
            <td> {{$cursos[$jornada->cursos_id -1]->nombre}} </td>
            <td> {{$jornada->fecha_ini}} </td>
            <td> {{$jornada->fecha_fin}} </td>
            <td> {{$jornada->num_sesiones}} </td>
            <td> {{$jornada->num_expediente}} </td>
            <td> {{$jornada->horario}} </td>
            <td> {{$docentes[$jornada->docentes_id - 1]->nombre}} {{$docentes[$jornada->docentes_id - 1]->apellido_1}} {{$docentes[$jornada->docentes_id - 1]->apellido_2}}</td>
            <td> {{$centros[$jornada->centros_id - 1]->nombre }} </td>
            <td> {{$aulas[$jornada->aulas_id - 1]->num}}</td>
            <td>

                <form method="POST" action="{{ url('/Jornadas/' .$jornada->id) }}" style="display:inline">

                    <a class="btn btn-success" href="{{ route('Jornadas.edit',$jornada->id) }}"> Editar </a>

                    @method('DELETE')

                    @csrf

                    @include('Jornadas.destroy')

                    <a href="" data-target="#modal-delete-{{$jornada->id}}" data-toggle="modal" class="btn btn-danger">Eliminar</a>

                    <a class="btn btn-info" href="{{ route('Jornadas.show',$jornada->id) }}"> Ver detalles </a>

                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div>

@endsection