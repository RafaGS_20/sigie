@extends('layouts.nosidebar')

@section('content')

<div class="container">
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h5> Creación de jornadas </h5>
            </div>
            <div class="card-body">
                <h5 class="card-title"> Instrucciones para la creación de jornadas </h5>
                <p class="card-text"> Para la creación de nuevas jornadas es necesario tener registrada previamente la información relacionada con la misma.
                    En las siguientes secciones podrán realizarse las consultas y gestiones necesarias para continuar con el proceso, todas estas gestiones se
                    abren en una nueva pestaña del navegador, una vez finalizado el proceso vuelva a esta pestaña para realizar la creación de la jornada. </p>
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="card text-white bg-dark mb-3 col-3">
            <div class="card-body">
                <h5 class="card-title"> Docentes </h5>
                <p class="card-text"> Gestión del profesorado que imparte los cursos en colaboración con la empresa. También es posible
                    gestionar sus titulaciones. </p>
                <a href="{{ url('Docentes') }}" target="_blank" class="btn btn-primary"> Consultar </a>
                <a href="{{ route('Docentes.create') }}" target="_blank" class="btn btn-primary"> Registrar </a>
            </div>
        </div>
        <div class="card text-white bg-success mb-3 col-3">
            <div class="card-body">
                <h5 class="card-title"> Cursos </h5>
                <p class="card-text"> Gestión de los cursos impartidos por la empresa.</p>
                <br><br><br>
                <a href="{{ url('Cursos') }}" target="_blank" class="btn btn-primary"> Consultar </a>
                <a href="{{ route('Cursos.create') }}" target="_blank" class="btn btn-primary"> Registrar </a>
            </div>
        </div>
        <div class="card text-white bg-dark mb-3 col-3">
            <div class="card-body">
                <h5 class="card-title"> Centros </h5>
                <p class="card-text"> Gestión de los centros en los que se imparten los cursos y se celebran estas jornadas. También es posible
                    gestionar las aulas de los centros.</p>
                <a href="{{ url('Centros') }}" target="_blank" class="btn btn-primary"> Consultar </a>
                <a href="{{ route('Centros.create') }}" target="_blank" class="btn btn-primary"> Registrar </a>
            </div>
        </div>
        <div class="card text-white bg-success mb-3 col-3">
            <div class="card-body">
                <h5 class="card-title"> Alumnos </h5>
                <p class="card-text"> Gestión de los alumnos que participan en cada curso o que han participado alguna vez en un curso impartido.</p>
                <br>
                <a href="{{ url('Alumnos') }}" target="_blank" class="btn btn-primary"> Consultar </a>
                <a href="{{ url('/Alumnosmasivo') }}" target="_blank" class="btn btn-primary"> Registrar </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="card col-12">
            <div class="card-header col-12">
                <h5 class="card-title"> Proceder a la creación de jornadas </h5>
            </div>
            <div class="card-body">
                <p class="card-text"> Si toda la información necesaria ya está registrada continúe mediante el siguiente enlace. </p>
                <a href="{{ route('Jornadas.create') }}" class="btn btn-success col-12"> Registrar nueva jornada de la empresa </a>
            </div>
        </div>
    </div>
</div>

@endsection