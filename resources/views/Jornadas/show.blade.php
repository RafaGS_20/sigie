@extends('layouts.nosidebar')

@section('content')

<div class="container">
    <div class="row">
        <table class="table table-striped table-bordered">
            <tr>
                <th> Cartel del curso </th>
                <th> Material </th>
                <th> Diplomas </th>
                <th> Evaluación </th>
                <th> Control de asistencia </th>
                <th> Fotos RGD </th>
                <th> Ficha del alumno </th>
            </tr>
            <tr>
                <td><a href="{{  url( '/cartel/' .$jornada->id ) }}"><i class="fas fa-book-open fa-2x"></i></a></td>
                <td><a href="{{  url( '/material/' .$jornada->id ) }}"><i class="fas fa-book fa-2x"></a></i></td>
                <td><a href="{{  url( '/diplomas/' .$jornada->id ) }}"><i class="fas fa-graduation-cap fa-2x"></a></i></td>
                <td><a href="{{  url( '/evaluacion/' .$jornada->id ) }}"><i class="far fa-check-square fa-2x"></a></i></td>
                <td><a href="{{  url( '/asistencia/' .$jornada->id ) }}"><i class="far fa-calendar-check fa-2x"></a></i></td>
                <td><a href="{{  url( '/fotosRGD/' .$jornada->id ) }}"><i class="fas fa-camera-retro fa-2x"></a></td>
                <td><a href="{{  url( '/registroalumno/' .$jornada->id ) }}"><i class="fas fa-clipboard-list fa-2x"></i></a></td>
            </tr>
        </table>
    </div>
    <div class="row">

        <h4> Datos de la jornada </h4>

        <table class="table table-striped table-bordered">
            <tr>
                <th colspan="3"> Jornadas de: {{$curso->nombre}} </th>
            </tr>
            <tr>
                <td> Número de sesiones: {{ $jornada->num_sesiones}} </td>
                <td> Fecha de inicio: {{ $jornada->fecha_ini}} </td>
                <td> Fecha de fin: {{ $jornada->fecha_fin}} </td>
            </tr>
        </table>

        <table class="table table-striped table-bordered">
            <tr>
                <th colspan=" {{count($sesiones)}} "> Fechas de las sesiones </th>
            </tr>
            <tr>
                @foreach ($sesiones as $sesion)
                <td> {{$sesion->fecha}} </td>
                @endforeach
            </tr>
        </table>
    </div>
    <div class="row">

        <h4> Datos del centro y aula </h4>

        <table class="table table-striped table-bordered">
            <tr>
                <th> Nombre </th>
                <th> Dirección </th>
                <th> Localidad </th>
                <th> Email </th>
                <th> Teléfono </th>
            </tr>
            <tr>
                <td> {{ $centro->nombre }} </td>
                <td> {{ $centro->calle_centro }}, número {{ $centro->num }}, piso {{ $centro->piso }} </td>
                <td> {{ $localidad_centro->localidad}} </td>
                <td> {{ $centro->email }} </td>
                <td> {{ $centro->telefono }} </td>
            </tr>
        </table>

        <table class="table table-striped table-bordered">
            <tr>
                <th> Número de aula </th>
                <th> Aforo </th>
                <th> Equipo </th>
                <th> Descripción </th>
            </tr>
            @foreach ($aulas as $aula)
            <tr>
                <td> {{$aula->num}} </td>
                <td> {{$aula->aforo}} </td>
                <td> {{$aula->equipo}} </td>
                <td> {{$aula->descripcion}} </td>
            </tr>
            @endforeach
        </table>
    </div>
    <div class="row">
        <h4> Datos del docente </h4>
        <table class="table table-striped table-bordered">
            <tr>
                <th> Nombre </th>
                <th> DNI </th>
                <th> Titulación </th>
                <th> Dirección </th>
                <th> Localidad </th>
                <th> Email </th>
                <th> Teléfono </th>
            </tr>
            <tr>
                <td> {{ $docente->nombre }} {{$docente->apellido_1}} {{$docente->apellido_2}}</td>
                <td> {{$docente->DNI}} </td>
                <td> {{$titulacion_docente->nombre}} </td>
                <td> {{$docente->direccion}} </td>
                <td> {{$localidad_docente->localidad}} </td>
                <td> {{$docente->email}} </td>
                <td> {{$docente->telefono}} </td>
            </tr>
        </table>
    </div>
    <div class="row">
        <h4> Datos de los alumnos </h4>
        <table class="table table-striped table-bordered">
            <tr>
                <th> Nombre </th>
                <th> DNI </th>
                <th> Nivel de estudios </th>
                <th> Datos de contacto </th>
                <th> Empresa </th>
                <th> Teléfono de la empresa </th>
            </tr>
            @foreach ($alumnos_empresas as $alumno_empresa)
            <tr>
                <td> {{$alumnos[ $alumno_empresa->alumnos_id -1]->nombre}}
                    {{$alumnos[ $alumno_empresa->alumnos_id -1]->apellido_1}}
                    {{$alumnos[ $alumno_empresa->alumnos_id -1]->apellido_2}}
                </td>
                <td> {{$alumnos[ $alumno_empresa->alumnos_id -1]->DNI}} </td>
                <td> {{$alumnos[ $alumno_empresa->alumnos_id -1]->nivel_estudios}}</td>
                <td> {{$alumnos[ $alumno_empresa->alumnos_id -1]->email}} /
                     {{$alumnos[ $alumno_empresa->alumnos_id -1]->telefono}}
                </td>
                <td> {{$empresas[ $alumno_empresa->empresas_id -1]->nombre}}</td>
                <td> {{$empresas[ $alumno_empresa->empresas_id -1]->telefono}}</td>
            </tr>
            @endforeach
        </table>
    </div>

</div>

@endsection