@foreach ($sesiones_jornadas as $sesion_jornada)

@extends('layouts.pdf')

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <div class="row">
                        <img src="{{asset('images/Logo-Fundae.png') }}" alt="Logo" width="300" height="100">
                    </div>

                    <div class="row justify-content-center">
                        <h4> {{ $curso }} </h4>
                    </div>

                    <div class="row justify-content-center">
                        <h4> CONTROL DE ASISTENCIA </h4>
                    </div>

                    <div class="row">
                        <table class='table' id="tablaPDF">
                            <tr>
                                <th colspan="3"> Nº ACCIÓN FORMATIVA: </th>
                                <th colspan="3"> GRUPO: </th>
                            </tr>
                            <tr>
                                <th> FECHA DE INICIO: </th>
                                <td colspan="2"> {{ $fecha_ini }} </td>
                                <th> FECHA DE FIN: </th>
                                <td colspan="2"> {{ $fecha_fin }} </td>
                            </tr>
                            <tr>
                                <th colspan="3"> DOCENTE RESPONSABLE: </th>
                                <td colspan="3"> {{ $docente }} </td>
                            </tr>
                            <tr>
                                <th> FECHA DE LA SESIÓN: </th>
                                <td> {{ $sesion_jornada->fecha}} </td>
                                <th> HORARIO DE IMPARTICIÓN:</th>
                                <td> {{ $horario }}</td>
                                <th> MODALIDAD:</th>
                                <td> {{ $modalidad }} </td>
                            </tr>
                            <tr>
                                <th colspan="3"> FIRMA DEL RESPONSABLE</th>
                                <td colspan="3"> </td>
                            </tr>
                        </table>
                    </div>

                    <div class="row">
                        <table class='table' id="tablaPDF">
                            <tr>
                                <th> Apellidos </th>
                                <th> Nombre </th>
                                <th colspan="2"> Firma </th>
                                <td> Observaciones</td>
                            </tr>
                            @foreach ($alumnos_empresas as $alumno_empresa)
                            <tr>
                                <td>
                                    {{$alumnos[ $alumno_empresa->alumnos_id -1]->apellido_1}}
                                    {{$alumnos[ $alumno_empresa->alumnos_id -1]->apellido_2}}
                                </td>
                                <td> {{$alumnos[ $alumno_empresa->alumnos_id -1]->nombre}} </td>
                                <td> </td>
                                <td colspan="2"> </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>

                    <div class="page-break"></div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

</body>

@endforeach