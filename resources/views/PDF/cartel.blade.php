@extends('layouts.pdf')

@section('content')

<div class="row">
    <img src="{{asset('images/Logo-Fundae.png') }}" alt="Logo" width="300" height="100">
</div>

<div class="row justify-content-center">
    <h4> {{ $curso }} </h4>
</div>

<div class="row">
    <table class='table' id="tablaPDF">
        <tr>
            <th colspan="2"> NÚMERO DE EXPEDIENTE:</th>
            <td colspan="2"> {{ $num_expediente }}</td>
        </tr>
        <tr>
            <th colspan="2"> ENTIDAD ORGANIZADORA:</th>
            <td colspan="2"> {{ $entidad_organizadora }}</td>
        </tr>
        <tr>
            <th colspan="2"> NOMBRE COMERCIAL:</th>
            <td colspan="2"> {{ $nombre_comercial }}</td>
        </tr>
        <tr>
            <th> FECHA DE INICIO: </th>
            <td> {{ $fecha_ini }} </td>
            <th> FECHA DE FIN: </th>
            <td> {{ $fecha_fin }} </td>
        </tr>
        <tr>
            <th> DURACIÓN DEL CURSO: </th>
            <td> {{ $horas }} </td>
            <th> MODALIDAD:</th>
            <td> {{ $modalidad }} </td>
        </tr>
        <tr>
            <th colspan="2"> Nº ACCIÓN FORMATIVA: </th>
            <th colspan="2"> GRUPO: </th>
        </tr>
        <tr>
            <th colspan="2"> DOCENTE</th>
            <td colspan="2"> {{ $docente }}</td>
        </tr>
    </table>
</div>

<div class="row">
    <table class='table' id="tablaPDF">
        <tr>
            <th colspan="2"> HORARIO DE IMPARTICIÓN:</th>
            <td colspan="2"> {{ $horario }}</td>
        </tr>
        <tr>
            <th colspan="2"> LUGAR DE IMPARTICIÓN:</th>
            <td colspan="2"> {{ $lugar }}</td>
        </tr>
        <tr>
    </table>
</div>

<div class="row">
    <p> Este curso {{$poliza}} requiere de una poliza de seguro adicional. </p>
</div>

<div class="row">
    <h5> OBJETIVOS DEL CURSO: </h5>
</div>

<div class="row">
    <p> {{$objetivos}} </p>
</div>

<div class="row">
    <h5 class="col-12"> CONTENIDOS DEL CURSO: </h5>
</div>

<div class="row">
    <p> {{$contenidos}} </p>
</div>

<div class="row">
    <h5> ASISTENCIA OBLIGATORIA: </h5>
</div>

@endsection