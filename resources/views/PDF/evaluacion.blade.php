@extends('layouts.pdf')

@section('content')

<div class="row">
    <img src="{{asset('images/Logo.jfif') }}" alt="Logo" width="300" height="100">
</div>

<div class="row justify-content-center">
    <h4> {{ $curso }} </h4>
</div>

<div class="row">
    <table class='table' id="tablaPDF">
        <tr>
            <th> FECHA DE INICIO: </th>
            <td> {{ $fecha_ini }} </td>
            <th> FECHA DE FIN: </th>
            <td> {{ $fecha_fin }} </td>
        </tr>
        <tr>
            <th> DOCENTE: </th>
            <td colspan="3"> {{ $docente }}</td>
        </tr>
    </table>
</div>

<div class="row">
    <h5> LISTADO DE EVALUACIÓN: </h5>
</div>

<div class="row">
    <table class='table' id="tablaPDF">
        <tr>
            <th> Apellidos </th>
            <th> Nombre </th>
            <th> Apto </th>
        </tr>
        @foreach ($alumnos_empresas as $alumno_empresa)
        <tr>
            <td>
                {{$alumnos[ $alumno_empresa->alumnos_id -1]->apellido_1}}
                {{$alumnos[ $alumno_empresa->alumnos_id -1]->apellido_2}}
            </td>
            <td> {{$alumnos[ $alumno_empresa->alumnos_id -1]->nombre}} </td>
            <td> </td>
        </tr>
        @endforeach
    </table>
</div>

@endsection