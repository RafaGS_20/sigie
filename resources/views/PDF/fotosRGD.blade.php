@foreach ($alumnos_empresas as $alumno_empresa)

@extends('layouts.pdf')

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <div class="row">
                        <img src="{{asset('images/Logo.jfif') }}" alt="Logo" width="300" height="120">
                    </div>

                    <div class="row justify-content-center">
                        <h5> AUTORIZACIÓN PARA LA PUBLICACIÓN DE IMÁGENES DE ALUMNOS POR NURIA DE LEÓN LÓPEZ - 78527107R (E2FORMACIÓN) </h5>
                    </div>

                    <div class="row">
                        <p> Con la inclusión de las nuevas tecnologías dentro de las comunicaciones, publicaciones y acciones comerciales que puede
                            realizar Nuria de León López – 78527107R (E2 Formación) y la posibilidad de que en estas puedan aparecer los datos personales
                            y/o imágenes que ha proporcionado a nuestra empresa dentro del vínculo comercial existente.
                            Y dado que el derecho a la propia imagen está reconocido al artículo 18 de la Constitución y regulado por la Ley 1/1982, de 5 de
                            mayo, sobre el derecho al honor, a la intimidad personal y familiar y a la propia imagen y el Reglamento (UE) 2016/679 del Parlamento
                            Europeo y del Consejo, de 27 de abril de 2016, relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos
                            personales y a la libre circulación de estos datos,
                            Nuria de León López – 78527107R (E2 Formación) pide su consentimiento para poder publicar los datos personales que nos ha facilitado o
                            imágenes en las cuales aparezcan individualmente o en grupo que con carácter comercial se puedan realizar con nuestra empresa.
                        </p>
                    </div>

                    <div class="row">
                        <p> Don/Doña: {{$alumnos[ $alumno_empresa->alumnos_id -1]->apellido_1}}
                            {{$alumnos[ $alumno_empresa->alumnos_id -1]->apellido_2}}
                            {{$alumnos[ $alumno_empresa->alumnos_id -1]->nombre}}
                            con DNI: {{$alumnos[ $alumno_empresa->alumnos_id -1]->DNI}}
                            autorizo a Nuria de León López – 78527107R (E2 Formación) a un uso comercial de mis datos personales facilitados dentro de la relación
                            comercial con nuestra empresa y para poder ser publicados en:
                        </p>
                    </div>
                    <div class="row">
                        <p> · La página web y perfiles en redes sociales de la empresa.</p>
                    </div>
                    <div class="row">
                        <p>· Fotografías para revistas o publicaciones de ámbito relacionado con el sector educativo.</p>
                    </div>

                    <div class="row">
                        <h5> En Puerto del Rosario a: </h5>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <h5> Firmado(Nombre y apellidos del alumno): </h5>
                    </div>

                    <div class="page-break"></div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

</body>

@endforeach