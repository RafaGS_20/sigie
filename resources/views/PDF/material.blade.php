@extends('layouts.pdf')

@section('content')

<div class="row">
    <img src="{{asset('images/Logo-Fundae.png') }}" alt="Logo" width="300" height="100">
</div>

<div class="row justify-content-center">
    <h4> {{ $curso }} </h4>
</div>

<div class="row">
    <table class='table' id="tablaPDF">
        <tr>
            <th colspan="2"> Nº ACCIÓN FORMATIVA: </th>
            <th colspan="2"> GRUPO: </th>
        </tr>
        <tr>
            <th> FECHA DE INICIO: </th>
            <td> {{ $fecha_ini }} </td>
            <th> FECHA DE FIN: </th>
            <td> {{ $fecha_fin }} </td>
        </tr>
    </table>
</div>

<div class="row">
    <h5> Descripción del material: </h5>
</div>

<div class="row">
    <table class='table' id="tablaPDF">
        <tr>
            <th> Apellidos </th>
            <th> Nombre </th>
            <th colspan="2"> Firma </th>
        </tr>
        @foreach ($alumnos_empresas as $alumno_empresa)
        <tr>
            <td>
                {{$alumnos[ $alumno_empresa->alumnos_id -1]->apellido_1}}
                {{$alumnos[ $alumno_empresa->alumnos_id -1]->apellido_2}}
            </td>
            <td> {{$alumnos[ $alumno_empresa->alumnos_id -1]->nombre}} </td>
            <td colspan="2"></td>
        </tr>
        @endforeach
    </table>
</div>

@endsection