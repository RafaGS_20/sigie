@foreach ($alumnos_empresas as $alumno_empresa)

@extends('layouts.pdf')

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <div class="row">
                        <img src="{{asset('images/Logo.jfif') }}" alt="Logo" width="300" height="120">
                    </div>

                    <div class="row justify-content-center">
                        <h5> FICHA DE INCRIPCIÓN </h5>
                    </div>

                    <div class="row">
                        <table class='table' id="tablaPDF">
                            <tr>
                                <th> NÚMERO DE EXPEDIENTE:</th>
                                <td> {{ $num_expediente }}</td>
                                <th> ENTIDAD ORGANIZADORA:</th>
                                <td> {{ $entidad_organizadora }}</td>
                            </tr>
                            <tr>
                                <th>Acción formativa:</th>
                                <td colspan="3"> {{ $curso }}</td>
                            </tr>
                            <tr>
                                <th> Comente su interés de participación en la acción formativa: </th>
                                <td colspan="3"> </td>
                            </tr>
                        </table>
                    </div>

                    <div class="row justify-content-center">
                        <h5> DATOS DEL PARTICIPANTE </h5>
                    </div>

                    <div class="row">
                        <table class='table' id="tablaPDF">
                            <tr>
                                <th> Apellidos y nombre </th>
                                <td colspan="2"> {{$alumnos[ $alumno_empresa->alumnos_id -1]->apellido_1}}
                                    {{$alumnos[ $alumno_empresa->alumnos_id -1]->apellido_2}}
                                    {{$alumnos[ $alumno_empresa->alumnos_id -1]->nombre}}
                                </td>
                            </tr>
                            <tr>
                                <td> DNI: {{$alumnos[ $alumno_empresa->alumnos_id -1]->DNI}} </td>
                                <td> SEXO: {{$alumnos[ $alumno_empresa->alumnos_id -1]->sexo}} </td>
                                <td> DISCAPACIDAD: SI NO</td>
                            </tr>
                            <tr>
                                <td colspan="2">Dirección: </td>
                                <td>Teléfono: </td>
                            </tr>
                            <tr>
                                <td> FECHA DE NACIMIENTO: {{$alumnos[ $alumno_empresa->alumnos_id -1]->fecha_nacimiento}}</td>
                                <td colspan="2"> CORREO: {{$alumnos[ $alumno_empresa->alumnos_id -1]->email}}</td>
                            </tr>
                            <tr>
                                <th colspan="3"> ESTUDIOS: </th>
                            </tr>
                            <tr>
                                <th colspan="2"> AREA FUNCIONAL: </th>
                                <th> CATEGORIA: </th>
                            </tr>
                            <tr>
                                <th colspan="3"> INSCRITO COMO DEMANDANTE DE EMPLEO: SI NO</th>
                            </tr>
                            <tr>
                                <th>FECHA: </th>
                                <th colspan="2">FIRMA: </th>
                            </tr>
                        </table>
                    </div>
                    <div class="page-break"></div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

</body>

@endforeach