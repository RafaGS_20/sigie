@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Sesiones de la jornada. </h2>
        </div>
    </div>
</div>

<br>

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{ route('Sesiones.store') }}" method="POST">
    @csrf
    <input type="hidden" name="jornadas_id" value="{{$jornada}}">
    @for ($i = 1; $i <= $num_sesiones; $i++) 
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong> Fecha de la sesión {{$i}}:</strong>
                    <input type="date" name="fecha_{{$i}}" class="form-control" placeholder="">
                </div>
            </div>
        </div>
    @endfor
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary col-2"> Dar de alta </button>
        </div>
</form>

@endsection