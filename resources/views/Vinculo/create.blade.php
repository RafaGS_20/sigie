@extends('layouts.main')

@section('content')

<script type="text/javascript">

</script>

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Vínculación de alumnos y empresas. </h2>
        </div>
    </div>
</div>

<br>

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{ route('Alumnos_Empresas_Jornadas.store') }}" method="POST">
    @csrf
    <input type="hidden" name="jornadas_id" value="{{$jornada}}">

    @for ($i = 1; $i <= $cantidad; $i++)
    <div class="row">
    <div class="col-xs-6 col-sm-6 col-md-6">
            <label for="cars">Elija un alumno:</label>
            <select name="alumnos_id_{{$i}}" id="alumnos_id">
                <option value="">Seleccione un dato</option>
                @foreach ($alumnos as $alumno)
                <option value="{{ $alumno->id }}"> {{ $alumno->nombre }} {{ $alumno->apellido_1 }} {{ $alumno->apellido_2 }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <label for="cars">Seleccione la empresa que lo envía:</label>
            <select name="empresas_id_{{$i}}" id="empresas_id">
            <option value="">Seleccione un dato</option>
                @foreach ($empresas as $empresa)
                <option value="{{ $empresa->id }}"> {{ $empresa->nombre }}</option>
                @endforeach
            </select>
        </div>
    </div>
    @endfor
    <br>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary col-2"> Dar de alta </button>
    </div>
</form>

@endsection