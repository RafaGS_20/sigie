@extends('layouts.main')

@section('content')

<div class="card bg-light mt-3">
    <div class="card-header">
        Sistema informático gestor de la información de la empresa.
    </div>
    <div class="card-body">
        <div class="row">
            <h5> Instrucciones para la la gestión de datos: </h5>
            <p> La gestión de los datos de la empresa se realiza desde el apartado actual. A través de la barra lateral podrá navegar por las principales tablas 
                de la base de datos de la aplicación. En cada apartado podrá realizar las siguientes gestiones: <br> 
                 - Creación. <br>
                 - Edición. <br>
                 - Eliminación. <br>
                Además, en algunos de los apartados podrá inspeccionar con detalle cada dato de la base, accediendo de esta forma a todos los otros posibles datos 
                relacionados con el mismo. <br>
                Algunas tablas no se muestran en la barra de navegación lateral para evitar sobrecargar la página de apartados, su gestión se realiza desde sus modelos 
                asociados. Por ejemplo: para la gestión de las aulas debe acceder al apartado "Centros".
            </p>
        </div>
        <div class="row">
            <h5> Creación de jornadas: </h5>
        </div>
        <div class="row">
            <p> Para la creación de las jornadas es necesario realizar una precarga de los datos de otros campos. Encontrará más información
                a la hora de realizar el proceso. Dicho proceso puede iniciarse desde el apartado "Jornadas" o desde el calendario de la aplicación. 
                Toda jornada registrada en la base de datos será accesible desde el calendario pulsando sobre ella y desde el apartado "Jornadas".  </p>
        </div>
    </div>
</div>

@endsection