
@extends('layouts.nosidebar')

@section('content')
<div class="row" id="inicio">

    <div id="externo">
        <div id="interno">
            <a href="gestiondedatos" style='text-decoration:none;color:white'>
                <i class="fas fa-database fa-5x"></i>
                <br>
                <br>
                <h1> Gestión de información </h1>
            </a>
        </div>
    </div>

    <div id="externo">
        <div id="interno">
            <a href="calendario" style='text-decoration:none;color:white'>
                <i class="far fa-calendar-alt fa-5x"></i>
                <br>
                <br>
                <h1> Gestión del calendario </h1>
            </a>
        </div>
    </div>

</div>
@endsection