@extends('layouts.main')

@section('content')

<div class="container">
    <div class="card bg-light mt-3">
        <div class="card-header">
            Sistema de importación y exportación masiva de alumnos.
        </div>
        <div class="card-body">
            <div class="row">
                <h5>Instrucciones para la importación: </h5>
                <p>Para realizar una subida masiva de alumnos a la base de datos, descargue la plantilla del siguiente enlace, cumplimente los datos de los alumnos,
                    suba el archivo a la página usando el botón de "Seleccionar archivo" y confirme con el botón verde. </p>
                <a href="{{ asset('docs/Alumnos.xlsx') }}"> Pinche aquí para descargar el archivo plantilla de subida. </a>
            </div>
            <div class="row">
                <h5>Instrucciones para la exportación: </h5>
                <p> Para descargar todos los datos de la base de datos a un Excel pulse el botón rojo. El archivo resultante puede utilizarlo para actualizar los datos
                de los alumnos subiéndolo a la página con el botón de "Seleccionar archivo" y confirmando el envío con el botón verde.  </p>
            </div>
            <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" name="file" class="form-control">
                <br>
                <button class="btn btn-success"> Cargar alumnos a la base de datos </button>
                <a class="btn btn-danger" href="{{ route('export') }}"> Descargar datos de alumnos</a>
            </form>
        </div>
    </div>
</div>

@endsection