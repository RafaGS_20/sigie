<!DOCTYPE html>
<html lang="es">

<head>

  @include('layouts.head')

</head>

<body>

  @include('layouts.navbar')

  @yield('content')

  @include('layouts.js')

</body>

</html>