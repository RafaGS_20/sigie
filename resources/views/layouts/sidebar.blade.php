    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href=" {{ url('gestiondedatos') }}">
        <div class="sidebar-brand-icon">
          <i class="fas fa-database"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Información de interés</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Tablas principales
      </div>

      <!-- Nav Item -->
      <li class="nav-item">
        <a class="nav-link" href="{{ url('Jornadas') }}">
          <i class="far fa-calendar-alt"></i>
          <span> Jornadas </span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ url('Docentes') }}">
          <i class="fas fa-user-tie"></i>
          <span> Docentes </span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ url('Centros') }}">
          <i class="fas fa-school"></i>
          <span> Centros </span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ url('Cursos') }}">
          <i class="fas fa-book-open"></i>
          <span> Cursos </span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ url('Alumnos') }}">
          <i class="fas fa-user-graduate"></i>
          <span> Alumnos </span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ url('Empresas') }}">
          <i class="fas fa-trademark"></i>
          <span> Empresas </span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Consultas de interés
      </div>



    </ul>
    <!-- End of Sidebar -->