<?php

use Illuminate\Support\Facades\Route;

// Controladores del apartado CRUD.
use App\Http\Controllers\JornadasController;
use App\Http\Controllers\DocentesController;
use App\Http\Controllers\CentrosController;
use App\Http\Controllers\CursosController;
use App\Http\Controllers\AlumnosController;
use App\Http\Controllers\EmpresasController;
use App\Http\Controllers\AulasController;
use App\Http\Controllers\SesionesController;
use App\Http\Controllers\TitulacionesController;
use App\Http\Controllers\Alumnos_Empresas_JornadasController;
use App\Http\Controllers\PDFController;


//Controlador del calendario. 
use App\Http\Controllers\FullCalendarController;

//Controlador necesario para la parte de importación y exportación. 
use App\Http\Controllers\ImportExportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Redireccionamiento al inicio en caso de error 404. 
Route::fallback(function () {

    return redirect('/inicio');

});
// Página inicial de nuestra aplicación: deriva en gestión de datos y en calendario. 
Route::get('/inicio', function () {
    return view('home');
});

// Página previa a la gestión de las jornadas y vinculación, permite gestionar información necesaria. 
Route::get('/intruccionesjornadas', function () {
    return view('Jornadas.previous');
});

// Control de importación y exportación, usado en ambos apartados de la aplicación. 
Route::get('Alumnosmasivo', [ImportExportController::class, 'importExportView']);
Route::get('export', [ImportExportController::class, 'export'])->name('export');
Route::post('import', [ImportExportController::class, 'import'])->name('import');

// Apartado 1 de la aplicación: gestión de datos, se compone de una vista inicial, que nos mete en el index de las tablas y en consultas seleccionadas. 
Route::get('/gestiondedatos', function () {
    return view('data');
});
// Para el funcionamiento de todo el método CRUD de los modelos y las diferentes consultas es necesario la utilización de sus controladores y rutas específicas. 
Route::resource('Jornadas', JornadasController::class);
Route::resource('Docentes', DocentesController::class);
Route::resource('Centros', CentrosController::class);
Route::resource('Cursos', CursosController::class);
Route::resource('Alumnos', AlumnosController::class);
Route::resource('Empresas', EmpresasController::class);
Route::resource('Aulas', AulasController::class);
Route::resource('Sesiones', SesionesController::class);
Route::resource('Titulaciones', TitulacionesController::class);
Route::resource('Alumnos_Empresas_Jornadas', Alumnos_Empresas_JornadasController::class);
Route::get('Jornadas/select_aulas/{centro}', [FullCalendarController::class, 'sacaAulas']);

// Apartado 2 de la aplicación: calendario, se compone de una vista inicial del calendario, así como de los métodos necesarios para la creación de las jornadas y su gestión. 
Route::get('/calendario', function () {
    return view('fullcalendar');
});
// Para el sistema de exportación a PDF es necesario un paquete extra.
Route::get('cartel/{id}', [PDFController::class, 'cartel']); // Generación del cartel del curso. 
Route::get('material/{id}', [PDFController::class, 'material']); // Generación del documento de material del curso. 
Route::get('diplomas/{id}', [PDFController::class, 'diplomas']); // Generación de los diplomas del curso. 
Route::get('evaluacion/{id}', [PDFController::class, 'evaluacion']); // Generación del control de aptos.
Route::get('asistencia/{id}', [PDFController::class, 'asistencia']); // Generación del control de asistencia del curso.  
Route::get('fotosRGD/{id}', [PDFController::class, 'fotosRGD']); // Generación del documento de autorización de imágenes.
Route::get('registroalumno/{id}', [PDFController::class, 'registroalumno']); // Generación del documento de registro del alumno. 